package org.coursera.androidcapstone.symptommanagementclient.test;

import org.coursera.androidcapstone.symptommanagement.client.activity.CheckinActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.HomeScreenActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.LoginActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.MainActivity;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.robotium.solo.Solo;

public class PatientWorkflowTest extends ActivityInstrumentationTestCase2<HomeScreenActivity> {

	private Solo solo;
	
	public PatientWorkflowTest() {
		this(PatientWorkflowTest.class.getSimpleName());
	}

	public PatientWorkflowTest(String name) {
		super(HomeScreenActivity.class);
		setName(name);		
	}
	
	protected static void setUpBeforeClass() throws Exception {
	}

	protected static void tearDownAfterClass() throws Exception {
	}

	protected void setUp() throws Exception {
		super.setUp();
		
        solo = new Solo(getInstrumentation(), getActivity());
		
	}

	protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
	}

	
	public void testPatientCheckin() {
	
		//Click on Login on home screen
		solo.assertCurrentActivity("Wrong activity", HomeScreenActivity.class);
		solo.clickOnButton("Login");
		
		//Go to login screen
		login("patient1");
		
		//Make sure we are on Checkin screen
		solo.assertCurrentActivity("Wrong activity", CheckinActivity.class);
		assertTrue(solo.searchText("How bad is your mouth pain?"));
		assertTrue(solo.searchText("Did you take your medications?"));

	}

	private void login(String loginName) {
		solo.assertCurrentActivity("Wrong activity", LoginActivity.class);
		Button loginButton = (Button)solo.getView(R.id.sign_in_button);
		String buttonText = loginButton.getText().toString();
		assertEquals("Log In", buttonText);
		EditText userNameText = (EditText)solo.getView(R.id.user_name);
		solo.enterText(userNameText, loginName);
		EditText userPasswordText = (EditText)solo.getView(R.id.user_password);
		solo.enterText(userPasswordText, "password1");
		solo.clickOnButton("Log In");
	}
	
}
