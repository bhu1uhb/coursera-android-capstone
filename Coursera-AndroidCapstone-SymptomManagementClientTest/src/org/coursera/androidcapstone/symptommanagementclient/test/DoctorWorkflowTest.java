package org.coursera.androidcapstone.symptommanagementclient.test;

import org.coursera.androidcapstone.symptommanagement.client.activity.HomeScreenActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.LoginActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.MedicationUpdateSelectionActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.PatientCheckinsListActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.PatientDetailsActivity;
import org.coursera.androidcapstone.symptommanagement.client.activity.PatientListActivity;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.robotium.solo.Solo;

/**
 * All of these need to be run with a clean database.
 *
 */
public class DoctorWorkflowTest extends ActivityInstrumentationTestCase2<HomeScreenActivity> {

	private Solo solo;
	
	public DoctorWorkflowTest() {
		this(DoctorWorkflowTest.class.getSimpleName());
	}

	public DoctorWorkflowTest(String name) {
		super(HomeScreenActivity.class);
		setName(name);		
	}
	
	protected static void setUpBeforeClass() throws Exception {
	}

	protected static void tearDownAfterClass() throws Exception {
	}

	protected void setUp() throws Exception {
		super.setUp();
		
        solo = new Solo(getInstrumentation(), getActivity());
		
	}

	protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
	}

	
	public void testDoctorUpdateCheckinMedicationPath() {
	
		//Click on Login on home screen
		solo.assertCurrentActivity("Wrong activity", HomeScreenActivity.class);
		solo.clickOnButton("Login");
		
		//Go to login screen
		login("doctor1");
		
		//Make sure we are on Patient List screen
		solo.assertCurrentActivity("Wrong activity", PatientListActivity.class);
		assertTrue(solo.searchText("Patient List"));
		assertTrue(solo.searchText("Doctor:"));

		//Go to Checkin List screen
		solo.clickInList(0);//first item in patient list
		solo.assertCurrentActivity("Wrong activity", PatientCheckinsListActivity.class);
		assertTrue(solo.searchText("Checkin List"));
		
		solo.clickOnButton("Update Medications");
		solo.assertCurrentActivity("Wrong activity", MedicationUpdateSelectionActivity.class);
		assertTrue(solo.searchText("LORTAB"));
		
		//TODO: Check proper items checked
		//get the list view
//		ListView list = (ListView)solo.getView(R.id.updateMedicationlistView);
//		//get the list element at the position you want
//		View rowView = list.getChildAt(0);
//		//get the Change button in that row and click on it
//		View buttonItem = rowView.findViewById(R.id.editPatientMedicationsButton); 
//	    solo.clickOnView(buttonItem);		
//		//Assert title on next screen
//		solo.assertCurrentActivity("Wrong activity", MedicationUpdateSelectionActivity.class);
//		assertTrue(solo.searchText("Select Medication"));
		
	}

	private void login(String loginName) {
		solo.assertCurrentActivity("Wrong activity", LoginActivity.class);
		Button loginButton = (Button)solo.getView(R.id.sign_in_button);
		String buttonText = loginButton.getText().toString();
		assertEquals("Log In", buttonText);
		EditText userNameText = (EditText)solo.getView(R.id.user_name);
		solo.enterText(userNameText, loginName);
		EditText userPasswordText = (EditText)solo.getView(R.id.user_password);
		solo.enterText(userPasswordText, "password1");
		solo.clickOnButton("Log In");
	}

	public void testDoctorViewCheckinPath() {

		//Click on Login on home screen
		solo.assertCurrentActivity("Wrong activity", HomeScreenActivity.class);
		solo.clickOnButton("Login");

		//Go to login screen
		login("doctor1");
		
		//Make sure we are on Patient List screen
		solo.assertCurrentActivity("Wrong activity", PatientListActivity.class);
		assertTrue(solo.searchText("Patient List"));
		assertTrue(solo.searchText("Doctor:"));

		//Go to Checkin List screen
		solo.clickInList(0);//first item in patient list
		solo.assertCurrentActivity("Wrong activity", PatientCheckinsListActivity.class);
		assertTrue(solo.searchText("Checkin List"));
		
		//Go to Checkin screen
		solo.clickInList(0);
		solo.assertCurrentActivity("Wrong activity", PatientDetailsActivity.class);
		assertTrue(solo.searchText("Checkin 1"));
		
		
	}
}
