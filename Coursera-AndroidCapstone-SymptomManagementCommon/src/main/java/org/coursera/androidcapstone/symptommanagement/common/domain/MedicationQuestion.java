package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="checkin_medication_question", schema="app")
@IdClass(MedicationQuestionPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicationQuestion implements Serializable, Identifiable<MedicationQuestionPK>, Nameable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="checkin_id", table="checkin_medication_question")
	private long checkinId;
	@Id
	@Column(name="medication_id", table="checkin_medication_question")
	private long medicationId;
	@Column(name="has_taken_medication", table="checkin_medication_question")
	private char hasTakenMedication = 'N';
	@Column(name="date_time_taken", table="checkin_medication_question")
	private Timestamp dateTimeMedicineTaken;

	@Transient
	private Medication medication;

	
	public MedicationQuestion() {
	}
	
	public MedicationQuestion(long checkinId, long medicationId) {
		this.checkinId = checkinId;
		this.medicationId = medicationId;
	}

	@Override
	@Transient
	public MedicationQuestionPK getId() {
		return new MedicationQuestionPK(checkinId, medicationId);
	}

	

	public long getCheckinId() {
		return checkinId;
	}

	public void setCheckinId(long checkinId) {
		this.checkinId = checkinId;
	}

	public long getMedicationId() {
		return medicationId;
	}

	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}


	public char getHasTakenMedication() {
		return hasTakenMedication;
	}

	public void setHasTakenMedication(char hasTakenMedication) {
		this.hasTakenMedication = hasTakenMedication;
	}

	public Timestamp getDateTimeMedicineTaken() {
		return dateTimeMedicineTaken;
	}

	public void setDateTimeMedicineTaken(Timestamp dateTimeMedicineTaken) {
		this.dateTimeMedicineTaken = dateTimeMedicineTaken;
	}

	public Medication getMedication() {
		return medication;
	}

	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	@Override
	public String toString() {
		return "MedicationQuestion [checkinId=" + checkinId + ", medicationId="
				+ medicationId + ", hasTakenMedication=" + hasTakenMedication
				+ ", dateTimeMedicineTaken=" + dateTimeMedicineTaken + "]";
	}

	@Override
	public CharSequence getName() {
		return new StringBuilder("Medication Questions for checkin").append(checkinId).append(" with medication ").append(medicationId).append(" taken on ").append(dateTimeMedicineTaken);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (checkinId ^ (checkinId >>> 32));
		result = prime
				* result
				+ ((dateTimeMedicineTaken == null) ? 0 : dateTimeMedicineTaken
						.hashCode());
		result = prime * result + hasTakenMedication;
		result = prime * result + (int) (medicationId ^ (medicationId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedicationQuestion other = (MedicationQuestion) obj;
		if (checkinId != other.checkinId)
			return false;
		if (dateTimeMedicineTaken == null) {
			if (other.dateTimeMedicineTaken != null)
				return false;
		} else if (!dateTimeMedicineTaken.equals(other.dateTimeMedicineTaken))
			return false;
		if (hasTakenMedication != other.hasTakenMedication)
			return false;
		if (medicationId != other.medicationId)
			return false;
		return true;
	}



}
