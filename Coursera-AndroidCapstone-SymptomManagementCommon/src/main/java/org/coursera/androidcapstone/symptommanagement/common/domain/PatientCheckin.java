package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="checkin", schema="app")
@SecondaryTable(name="checkin_questions", schema="app")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PatientCheckin implements Serializable, Identifiable<Long>, Nameable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="checkin_id")
	private long checkinId;
	@Column(name="patient_id")
	private long patientId;
	@Column(name="checkin_datetime")
	private Timestamp checkinDateTime;
	
	@Column(name="mouth_throat_pain", table="checkin_questions")
	private String mouthThroatPain;
	@Column(name="does_stop_eat_drink", table="checkin_questions")
	private String doesStopEatDrink;
	
	@Transient
	private List<MedicationQuestion> medicationQuestions;
	@Transient
	private boolean isAlertable = false;
	
	public PatientCheckin() {
	}
	
	@Override
	@Transient
	public Long getId() {
		return checkinId;
	}

	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public long getCheckinId() {
		return checkinId;
	}
	public void setCheckinId(long checkinId) {
		this.checkinId = checkinId;
	}
	public List<MedicationQuestion> getMedicationQuestions() {
		return medicationQuestions;
	}
	public void setMedicationQuestions(List<MedicationQuestion> medicationQuestions) {
		this.medicationQuestions = medicationQuestions;
	}

	public Timestamp getCheckinDateTime() {
		return checkinDateTime;
	}

	public void setCheckinDateTime(Timestamp checkinDateTime) {
		this.checkinDateTime = checkinDateTime;
	}

	public String getMouthThroatPain() {
		return mouthThroatPain;
	}

	public void setMouthThroatPain(String mouthThroatPain) {
		this.mouthThroatPain = mouthThroatPain;
	}

	public String getDoesStopEatDrink() {
		return doesStopEatDrink;
	}

	public void setDoesStopEatDrink(String doesStopEatDrink) {
		this.doesStopEatDrink = doesStopEatDrink;
	}

	public boolean isAlertable() {
		return isAlertable;
	}

	public void setAlertable(boolean isAlertable) {
		this.isAlertable = isAlertable;
	}

	@Override
	public String toString() {
		return "PatientCheckin [checkinId=" + checkinId + ", patientId="
				+ patientId + ", checkinDateTime=" + checkinDateTime
				+ ", mouthThroatPain=" + mouthThroatPain
				+ ", doesStopEatDrink=" + doesStopEatDrink
				+ ", medicationQuestions=" + medicationQuestions + "]";
	}

	@Override
	public CharSequence getName() {
		return new StringBuilder("Checkin ").append(checkinId).append(" for patient ").append(patientId).append(" done on ").append(checkinDateTime);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((checkinDateTime == null) ? 0 : checkinDateTime.hashCode());
		result = prime * result + (int) (checkinId ^ (checkinId >>> 32));
		result = prime
				* result
				+ ((doesStopEatDrink == null) ? 0 : doesStopEatDrink.hashCode());
		result = prime * result + (isAlertable ? 1231 : 1237);
		result = prime * result
				+ ((mouthThroatPain == null) ? 0 : mouthThroatPain.hashCode());
		result = prime * result + (int) (patientId ^ (patientId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatientCheckin other = (PatientCheckin) obj;
		if (checkinDateTime == null) {
			if (other.checkinDateTime != null)
				return false;
		} else if (!checkinDateTime.equals(other.checkinDateTime))
			return false;
		if (checkinId != other.checkinId)
			return false;
		if (doesStopEatDrink == null) {
			if (other.doesStopEatDrink != null)
				return false;
		} else if (!doesStopEatDrink.equals(other.doesStopEatDrink))
			return false;
		if (isAlertable != other.isAlertable)
			return false;
		if (mouthThroatPain == null) {
			if (other.mouthThroatPain != null)
				return false;
		} else if (!mouthThroatPain.equals(other.mouthThroatPain))
			return false;
		if (patientId != other.patientId)
			return false;
		return true;
	}

}
