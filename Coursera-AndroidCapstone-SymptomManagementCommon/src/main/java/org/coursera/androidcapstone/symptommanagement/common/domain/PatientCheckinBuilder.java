package org.coursera.androidcapstone.symptommanagement.common.domain;


import java.sql.Timestamp;
import java.util.List;

public class PatientCheckinBuilder {

	private long patientId;
	private long checkinId;
	private Timestamp checkinDateTime;
	private String mouthThroatPain;
	private String doesStopEatDrink;
	private List<MedicationQuestion> medicationQuestions;

	
	private PatientCheckinBuilder(long checkinId) {
		this.checkinId = checkinId;
	}
	
	public static PatientCheckinBuilder newBuilder(long checkinId) { 
		return new PatientCheckinBuilder(checkinId); 
	}

	public PatientCheckinBuilder patientId(long value) {
		this.patientId = value;
		return this;
	}

	public PatientCheckinBuilder checkinDateTime(Timestamp value) {
		this.checkinDateTime = value;
		return this;
	}

	public PatientCheckinBuilder mouthThroatPain(String value) {
		this.mouthThroatPain = value;
		return this;
	}

	public PatientCheckinBuilder doesStopEatDrink(String value) {
		this.doesStopEatDrink = value;
		return this;
	}

	public PatientCheckinBuilder medicationQuestions(List<MedicationQuestion> value) {
		this.medicationQuestions = value;
		return this;
	}

	public PatientCheckin build() { 
		PatientCheckin result = new PatientCheckin(); 
		
		result.setPatientId(patientId);
		result.setCheckinId(checkinId);
		result.setCheckinDateTime(checkinDateTime);
		result.setMouthThroatPain(mouthThroatPain);
		result.setDoesStopEatDrink(doesStopEatDrink);
		result.setMedicationQuestions(medicationQuestions); 
		return result;
	}

	public static PatientCheckinBuilder buildUpon(PatientCheckin original) {
		PatientCheckinBuilder builder = newBuilder(original.getCheckinId());
		builder.patientId(original.getPatientId());
		builder.checkinDateTime(original.getCheckinDateTime());
		builder.mouthThroatPain(original.getMouthThroatPain());
		builder.doesStopEatDrink(original.getDoesStopEatDrink());
		builder.medicationQuestions(original.getMedicationQuestions());
		return builder;
	}
}