

package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="USER_CREDENTIALS", schema="APP")
@SecondaryTable(name="USER_DETAILS", schema="APP")
@DiscriminatorValue("DR")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Doctor extends User {

	private static final long serialVersionUID = 1L;

	@Transient
	private List<Patient> patientList;
	
	public Doctor() {
	}

	public Doctor(long userId, String userTypeCode) {
		super(userId, userTypeCode);
	}

	public List<Patient> getPatientList() {
		return patientList;
	}

	public void setPatientList(List<Patient> patientList) {
		this.patientList = patientList;
	}

	@Override
	public UserType getUserType() {
		return UserType.DOCTOR;
	}

	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
