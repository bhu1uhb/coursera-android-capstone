package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="medication", schema="app")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Medication implements Serializable, Identifiable<Long>, Nameable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="medication_id")
	private long medicationId;
	@Column(name="medication_name")
	private String medicationName;
	
	public Medication() {
	}
	
	public Medication(long mediationId, String medicationName) {
		this.medicationId = mediationId;
		this.medicationName = medicationName;
	}

	@Override
	@Transient
	public Long getId() {
		return medicationId;
	}
	
	public long getMedicationId() {
		return medicationId;
	}
	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}
	public String getMedicationName() {
		return medicationName;
	}
	public void setMedicationName(String medicationName) {
		this.medicationName = medicationName;
	}


	@Override
	public String toString() {
		return "Medication [medicationId=" + medicationId + ", medicationName="
				+ medicationName + "]";
	}

	@Override
	public CharSequence getName() {
		return medicationName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (medicationId ^ (medicationId >>> 32));
		result = prime * result
				+ ((medicationName == null) ? 0 : medicationName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medication other = (Medication) obj;
		if (medicationId != other.medicationId)
			return false;
		if (medicationName == null) {
			if (other.medicationName != null)
				return false;
		} else if (!medicationName.equals(other.medicationName))
			return false;
		return true;
	}
	
}
