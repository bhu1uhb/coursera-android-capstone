package org.coursera.androidcapstone.symptommanagement.common.domain;

import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;

public enum PainSeverityLevel implements Identifiable<String>{

	WELL_CONTROLLED("WLC", "Well-Controlled"),
	MODERATE("MOD", "Moderate"),
	SEVERE("SEV", "Severe")
	;
	private final String code;
	private final String displayName;
	
	private PainSeverityLevel(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}

	public String getCode() {
		return code;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	@Override
	public String toString() {
		return displayName;
	}
	
	public String getId() {
		return code;
	}
	
	public static PainSeverityLevel findSeverityLevelByCode(String code) {
		return ApplicationUtils.findEnumById(PainSeverityLevel.class, code);
	}
	
}
