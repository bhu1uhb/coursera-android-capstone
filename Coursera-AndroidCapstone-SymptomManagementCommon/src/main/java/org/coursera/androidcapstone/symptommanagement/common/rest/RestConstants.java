package org.coursera.androidcapstone.symptommanagement.common.rest;

public class RestConstants {

	
	public static final String HOST_URL = "http://localhost:8080";

	//PathVariables
	
	/* User endpoints */
	public static final String USER_RESOURCE_NAME = "user";	
	public static final String USER_ENDPOINT_PATH = "/" + USER_RESOURCE_NAME;
	public static final String USER_FIND_REST_ENDPOINT_PATH = USER_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.USER_ID;
	public static final String USER_FIND_BY_CREDENTIALS_ENDPOINT_PATH = USER_ENDPOINT_PATH + "/login";
	public static final String USER_FIND_BY_CREDENTIALS_REST_PATH = 
			USER_FIND_BY_CREDENTIALS_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.LOGIN_NAME
			+ SymptomManagementPathVariable.RestPath.LOGIN_PWD;
	
	/* Patient endpoints */
	public static final String PATIENT_RESOURCE_NAME = "patient";	
	public static final String PATIENT_ENDPOINT_PATH = "/" + PATIENT_RESOURCE_NAME;
	//Inserts a new checkin 
	public static final String PATIENT_CHECKIN_REST_PATH = PATIENT_ENDPOINT_PATH + "/checkin";
	//Finds a checkin 
	public static final String PATIENT_FIND_CHECKIN_REST_PATH = PATIENT_CHECKIN_REST_PATH + SymptomManagementPathVariable.RestPath.CHECKIN_ID;
	//Finds medications 
	public static final String PATIENT_FIND_MEDICATIONS_ENDPOINT = PATIENT_ENDPOINT_PATH + "/medication";
	public static final String PATIENT_FIND_MEDICATIONS_REST_PATH = PATIENT_FIND_MEDICATIONS_ENDPOINT + SymptomManagementPathVariable.RestPath.PATIENT_ID;
	//TODO: Will this require a service or just be done client side???
	public static final String PATIENT_REMINDER_REST_PATH = PATIENT_ENDPOINT_PATH + "/reminder";
	
	
	/* Doctor endpoints */
	public static final String DOCTOR_RESOURCE_NAME = "doctor";
	public static final String DOCTOR_ENDPOINT_PATH = "/" + DOCTOR_RESOURCE_NAME;
	//find patients
	public static final String DOCTOR_PATIENTS_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/patients";
	public static final String DOCTOR_FIND_PATIENTS_REST_PATH = DOCTOR_PATIENTS_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.DOCTOR_ID;
	//find patient by fullname
	public static final String DOCTOR_PATIENT_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/patient";
	public static final String DOCTOR_PATIENT_SEARCH_FULLNAME_ENDPOINT = DOCTOR_PATIENT_ENDPOINT_PATH;
	public static final String DOCTOR_PATIENT_SEARCH_FULLNAME_REST_PATH = DOCTOR_PATIENT_SEARCH_FULLNAME_ENDPOINT + SymptomManagementPathVariable.RestPath.DOCTOR_ID + SymptomManagementPathVariable.RestPath.USER_FULLNAME;

	//Find all checkins for a patient
	public static final String DOCTOR_CHECKINS_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/checkins";
	public static final String DOCTOR_FIND_CHECKINS_REST_PATH = DOCTOR_CHECKINS_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.PATIENT_ID;
	//Find a checkin based on checkinId
	public static final String DOCTOR_CHECKIN_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/checkin";
	public static final String DOCTOR_FIND_CHECKIN_REST_PATH = DOCTOR_CHECKIN_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.CHECKIN_ID;
	//Assign a medication to a patient
	public static final String DOCTOR_ASSIGNMED_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/assignMed"; 
	public static final String DOCTOR_ASSIGN_MEDICATION_REST_PATH = DOCTOR_ASSIGNMED_ENDPOINT_PATH;
	//Logically delete a patient's medication assignment
	public static final String DOCTOR_REMOVE_ASSIGNMED_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/removeAssignedMed"; 
	public static final String DOCTOR_REMOVE_ASSIGNED_MEDICATION_REST_PATH = DOCTOR_REMOVE_ASSIGNMED_ENDPOINT_PATH;
	//Represents all alarms for all of a doctor's patients
	//For updating an alarm
	public static final String DOCTOR_ALARM_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/alarm";
	//Check to see if there are any alarms for a doctor
	public static final String DOCTOR_ALARMS_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/alarms";
	public static final String DOCTOR_CHECK_ALL_PATIENTS_ALARMS_REST_PATH = DOCTOR_ALARMS_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.DOCTOR_ID;
	//find all medications 
	public static final String DOCTOR_MEDICATIONS_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/medications";
	//find patient medications
	public static final String DOCTOR_FIND_PATIENT_MEDICATIONS_ENDPOINT_PATH = DOCTOR_ENDPOINT_PATH + "/medication"; 
	public static final String DOCTOR_FIND_PATIENT_MEDICATIONS_REST_PATH = DOCTOR_FIND_PATIENT_MEDICATIONS_ENDPOINT_PATH + SymptomManagementPathVariable.RestPath.PATIENT_ID;
	
}
