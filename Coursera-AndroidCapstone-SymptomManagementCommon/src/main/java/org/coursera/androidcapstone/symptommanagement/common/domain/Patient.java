package org.coursera.androidcapstone.symptommanagement.common.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="USER_CREDENTIALS", schema="APP")
@SecondaryTable(name="USER_DETAILS", schema="APP")
@DiscriminatorValue("PA")
public class Patient extends User implements Comparable<Patient> {

	private static final long serialVersionUID = 1L;
	
	@Transient
	private int totalAlertCount = 0;
	@Transient
	private int activeAlertCount = 0;
	
	public Patient() {
	}

	public Patient(long userId, String userTypeCode) {
		super(userId, userTypeCode);
	}

	public int getTotalAlertCount() {
		return totalAlertCount;
	}

	public void setTotalAlertCount(int totalAlertCount) {
		this.totalAlertCount = totalAlertCount;
	}

	public int getActiveAlertCount() {
		return activeAlertCount;
	}

	public void setActiveAlertCount(int activeAlertCount) {
		this.activeAlertCount = activeAlertCount;
	}

	@Override
	public UserType getUserType() {
		return UserType.PATIENT;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public int compareTo(Patient compare) {
		return totalAlertCount == compare.getTotalAlertCount() ? 0 : totalAlertCount < compare.getTotalAlertCount() ? -1 : 1 ;
	}
}
