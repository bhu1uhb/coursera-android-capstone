package org.coursera.androidcapstone.symptommanagement.common.domain;

import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;

public enum DoctorAlertCriteria implements Identifiable<String>{

	SEVERE_PAIN_12("SEV","12 hours of severe pain"),
	MODERATE_TO_SEVERE_PAIN_16("MOD","16 hours of moderate to severe pain"),
	CANT_EAT_12("CNT","12 hours of 'I can�t eat'")
	;
	private final String displayName;
	private final String code;
	
	private DoctorAlertCriteria(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getCode() {
		return code;
	}

	public String getId() {
		return code;
	}
	
	public static DoctorAlertCriteria findAlertCriteriaByCode(String code) {
		return ApplicationUtils.findEnumById(DoctorAlertCriteria.class, code);
	}

}
