package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="USER_CREDENTIALS", schema="APP")
@SecondaryTable(name="USER_DETAILS", schema="APP")
@DiscriminatorColumn(name="USER_TYPE", discriminatorType=DiscriminatorType.STRING)
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable, Identifiable<Long>, Nameable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="user_id", table="USER_CREDENTIALS")
	protected Long userId;
	@Column(name="login_name", table="USER_CREDENTIALS")
	protected String loginName;
	@Column(name="login_pwd", table="USER_CREDENTIALS")
	protected String loginPassword;
	@Column(name="user_lastname", table="USER_DETAILS")
	protected String lastName;
	@Column(name="user_firstname", table="USER_DETAILS")
	protected String firstName;
	@Column(name="user_phone", table="USER_DETAILS")
	protected String phoneNumber;
	@Column(name="user_email", table="USER_DETAILS")
	protected String emailAddress;
	@Column(name="user_type", table="USER_DETAILS")
	protected String userTypeCode; 
	@Column(name="user_birthdate", table="USER_DETAILS")
	protected Date birthDate;	

	public User() {
	}
	
	public User(long userId, String userTypeCode) {
		this.userId = userId;
		this.userTypeCode = userTypeCode;
	}
	
	@Override
	@Transient
	public Long getId() {
		return userId;
	}

	@Transient
	public UserType getUserType() {
		return userTypeCode != null ? UserType.findUserTypeByCode(userTypeCode) : null; 
	}

	@Transient
	public String getFullName() {
		return firstName + " " + lastName;
	}

	public Long getUserId() {
		return userId;
	}


	public void setUserId(Long userId) {
		this.userId = userId;
	}


	public String getLoginName() {
		return loginName;
	}


	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLoginPassword() {
		return loginPassword;
	}


	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}


	public String getUserTypeCode() {
		return userTypeCode;
	}


	public void setUserTypeCode(String userTypeCode) {
		this.userTypeCode = userTypeCode;
	}


	public Date getBirthDate() {
		return birthDate;
	}


	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	@Override
	public String toString() {
		return "User [userId=" + userId + ", loginName=" + loginName
				+ ", loginPassword=" + loginPassword + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", phoneNumber=" + phoneNumber
				+ ", emailAddress=" + emailAddress + ", userTypeCode="
				+ userTypeCode + ", birthDate=" + birthDate + "]";
	}

	@Override
	public CharSequence getName() {
		return getFullName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((loginName == null) ? 0 : loginName.hashCode());
		result = prime * result
				+ ((loginPassword == null) ? 0 : loginPassword.hashCode());
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result
				+ ((userTypeCode == null) ? 0 : userTypeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (loginName == null) {
			if (other.loginName != null)
				return false;
		} else if (!loginName.equals(other.loginName))
			return false;
		if (loginPassword == null) {
			if (other.loginPassword != null)
				return false;
		} else if (!loginPassword.equals(other.loginPassword))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userTypeCode == null) {
			if (other.userTypeCode != null)
				return false;
		} else if (!userTypeCode.equals(other.userTypeCode))
			return false;
		return true;
	}



}
