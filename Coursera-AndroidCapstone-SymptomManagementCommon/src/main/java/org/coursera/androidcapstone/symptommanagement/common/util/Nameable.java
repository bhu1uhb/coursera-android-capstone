package org.coursera.androidcapstone.symptommanagement.common.util;

public interface Nameable {

	public CharSequence getName();
}
