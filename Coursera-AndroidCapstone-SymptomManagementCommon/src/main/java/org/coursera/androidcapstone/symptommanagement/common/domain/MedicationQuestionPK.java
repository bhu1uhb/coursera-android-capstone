package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;


public class MedicationQuestionPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long checkinId;
	private long medicationId;

	public MedicationQuestionPK() {
	}
	
	public MedicationQuestionPK(long checkinId, long medicationId) {
		this.checkinId = checkinId;
		this.medicationId = medicationId;
	}

	public long getCheckinId() {
		return checkinId;
	}

	public void setCheckinId(long checkinId) {
		this.checkinId = checkinId;
	}

	public long getMedicationId() {
		return medicationId;
	}

	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (checkinId ^ (checkinId >>> 32));
		result = prime * result + (int) (medicationId ^ (medicationId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedicationQuestionPK other = (MedicationQuestionPK) obj;
		if (checkinId != other.checkinId)
			return false;
		if (medicationId != other.medicationId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MedicationQuestionPK [checkinId=" + checkinId
				+ ", medicationId=" + medicationId + "]";
	}

}
