package org.coursera.androidcapstone.symptommanagement.common.util;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.coursera.androidcapstone.symptommanagement.common.constant.CommonConstants;

public class ApplicationUtils {

	private ApplicationUtils(){
	}
	
	/**
	 * Allows an {@link Enum} instance that implements {@link Identifiable} to be found using the identifier
	 * method (getId()).
	 * <p>
	 * If the {@link Identifiable} implementation denotes a identifier that is a {@link String}, then the
	 * comparison ignores case.
	 * </p>
	 * @param clazz The {@link Enum} class
	 * @param id The identifier returned from the getId() call of the enum that implements {@link Identifiable}.
	 * @return The {@link Enum} instance identified using the id parameter.
	 * @throws IllegalArgumentException If the identifier parameters is null or contains a value that cannot
	 * identify an enum instance of the clazz parameter.
	 */
	public static <E extends Enum<E> & Identifiable<K>, K> E findEnumById(Class<E> clazz, K id) {
			E e = null;
			E[] values = clazz.getEnumConstants();
			for (E value : values) {
				if (id instanceof String) {
					String strId = (String)id;
					String strValueId = (String)value.getId();
					if(strValueId.equalsIgnoreCase(strId)) {
						e = value;
						break;
					}					
				} else {
					if(value.getId().equals(id)) {
						e = value;
						break;
					}
				}
			}
			if (e == null) {
				throw new IllegalArgumentException("A value of class " + clazz.getSimpleName() + " cannot be found using the ID " + id);
			}
			return e;
	}
	
	/**
	 * Converts a date expressed as a String to an equivalent {@link java.sql.Date} object.
	 * 
	 * @param strDate The date as a String to be converted.
	 * @param dateFormat optional date format. Uses MM/dd/yyyy if format is not given.
	 * @return 
	 * @throws IllegalArgumentException if the date String or format is illegal.
	 */
	public static Date convertStringToSqlDate(String strDate, String... dateFormat) {
		if (dateFormat == null || dateFormat.length == 0) {
			dateFormat = new String[]{"MM/dd/yyyy"};
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat[0]); 
		java.util.Date date = null;
		try {
			date = sdf.parse(strDate);
		} catch (ParseException e) {
			throw new IllegalArgumentException("Date " + strDate + " is an illegal date of format " + dateFormat, e);
		}
		if (date == null) {
			date = new java.util.Date();			
		}
		return new Date(date.getTime());
	}

	
	public static Timestamp convertSqlTimeToTodaysTimestamp(Time time) {
		long midnightMiliDiff = getMidnightLastNightAsLong() - getMidnightTonightAsLong();
		long timeToday = time.getTime() + midnightMiliDiff;
		return new Timestamp(timeToday);
	}
	
	public static Timestamp convertSqlTimeToTomorrowsTimestamp(Time time) {
		System.out.println("Time: " + time);
		System.out.println("Time milis: " + time.getTime());
		
		long midnightMiliDiff = getMidnightLastNightAsLong() + CommonConstants.MILIS_PER_DAY ;
		long timeToday = time.getTime();//
		return new Timestamp(timeToday);
	}
	
	/**
	 * Calculates whether the absolute value of the time difference between two timestamps is greater 
	 * than a comparison number of hours.
	 * 
	 * @param ts1 first timestamp
	 * @param ts2 second timestamp to compare with the first
	 * @param diffComparehHrs the number of hours to compare the two timestamps.
	 * @return
	 */
	public static boolean isDifferenceGreaterThanHrs(Timestamp ts1, Timestamp ts2, int diffComparehHrs) {
		long time1 = ts1.getTime();
		long time2 = ts2.getTime();
		long diff = Math.abs(time1 - time2);
		long hrsInMillis = Long.valueOf(diffComparehHrs * CommonConstants.MILIS_PER_HOUR);		
		return diff > hrsInMillis;
	}

	
	/**
	 * Converts a timestamp expressed as a String to an equivalent {@link java.sql.Timestamp} object.
	 * 
	 * @param strDate The date as a String to be converted.
	 * @param dateFormat optional date format. Uses MM/dd/yyyy hh:mm:ss if format is not given.
	 * @return 
	 * @throws IllegalArgumentException if the date String or format is illegal.
	 */
	public static Timestamp convertStringToSqlTimestamp(String strDate, String... timestampFormat) {
		if (timestampFormat == null || timestampFormat.length == 0) {
			timestampFormat = new String[]{"MM/dd/yyyy hh:mm:ss"};
		}
		SimpleDateFormat sdf = new SimpleDateFormat(timestampFormat[0]); 
		java.util.Date date = null;
		try {
			date = sdf.parse(strDate);
		} catch (ParseException e) {
			throw new IllegalArgumentException("Date " + strDate + " is an illegal timestamp of format " + timestampFormat, e);
		}
		if (date == null) {
			date = new java.util.Date();			
		}
		return new Timestamp(date.getTime());
	}

	
	public static Timestamp getNextTimestamp(Timestamp[] times, Timestamp now) {
		Timestamp nextTime = now;
		Timestamp minTime = times[0];
		//get all times after now
		List<Timestamp> timeList = new ArrayList<Timestamp>();
		for (Timestamp timePref : times) {
			if (timePref.after(now)) {
				timeList.add(timePref);
			}
			//get min time to use if no time after now is found
			Timestamp ltimePref = timePref;
			if (ltimePref.before(minTime)) {
				minTime = ltimePref;
			}
		}
		//if there are times after 'now', find next time
		if (!timeList.isEmpty()) {
			nextTime = timeList.get(0);
			for (int i = 1; i < timeList.size(); i++) {
				Timestamp next = timeList.get(i); 
				if (next.before(nextTime)) {
					nextTime = next;
				}
			}
		//if there are no times after now, then select tomorrow's time 	
		} else {
			//To convert time to tomorrow's time, so just add 24 hours to min time
			nextTime = getSameTimeTomorrow(minTime);
		}
		
		return nextTime;
	}

	
	public static Timestamp getNextTimestamp(Timestamp[] times) {
		java.util.Date now = new java.util.Date();
		return getNextTimestamp(times, new Timestamp(now.getTime()));
	}

	
	public static long getMidnightTonightAsLong(java.util.Date now) {
		long midnight = 0;
		// today    
		Calendar date = new GregorianCalendar();
		date.setTime(now);
		//
		// reset hour, minutes, seconds and millis
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);

		// next day
		date.add(Calendar.DAY_OF_MONTH, 1);		
		midnight = date.getTime().getTime(); 
		return midnight;
	}

	public static long getMidnightTonightAsLong() {
		return getMidnightTonightAsLong(new java.util.Date());
	}
	
	public static long getMidnightLastNightAsLong(java.util.Date now) {
		long midnight = 0;
		// today    
		Calendar date = new GregorianCalendar();
		date.setTime(now);
		//
		// reset hour, minutes, seconds and millis
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);

		midnight = date.getTime().getTime(); 
		return midnight;
	}

	public static long getMidnightLastNightAsLong() {
		return getMidnightLastNightAsLong(new java.util.Date());
	}
	

//	public static long getMidnightLastNightAsLong(java.util.Date now) {
//		long midnight = 0;
//		// today    
//		Calendar date = new GregorianCalendar();
//		date.setTime(now);
//		//
//		// reset hour, minutes, seconds and millis
//		date.set(Calendar.HOUR_OF_DAY, 0);
//		date.set(Calendar.MINUTE, 0);
//		date.set(Calendar.SECOND, 0);
//		date.set(Calendar.MILLISECOND, 0);
//
//		midnight = date.getTime().getTime(); 
//		return midnight;
//	}

	public static Timestamp convertTimeStringToSqlTimestamp(String strTime) {
		Calendar date = new GregorianCalendar();
		return convertTimeStringToSqlTimestamp(date, strTime);
	}

	public static Timestamp convertTimeStringToSqlTimestamp(Calendar date, String strTime) {
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = date.get(Calendar.MONTH);
		int year = date.get(Calendar.YEAR);
		String strTs = String.format("%d/%d/%d %s", month, day, year,strTime);
		System.out.println("Today's time String: " + strTs);
		return convertStringToSqlTimestamp(strTs, "MM/dd/yyyy hh:mm:ss");
		
	}	
	public static Timestamp getSameTimeTomorrow(Timestamp tsToday) {
		Calendar date = new GregorianCalendar();
		return getSameTimeTomorrow(date, tsToday);
	}

	public static Timestamp getSameTimeTomorrow(Calendar date, Timestamp tsToday) {
		date.setTime(new Date(tsToday.getTime()));
		// next day
		date.add(Calendar.DAY_OF_WEEK, 1);		
		return new Timestamp(date.getTime().getTime());
	}
	
	/**
	 * Converts a list that implements {@link Identifiable} into a {@link Map}.
	 * 
	 * @param list a list whose elements implement {@link Identifiable}.
	 * @return
	 */
	public static <K, V extends Identifiable<K>> Map<K, V> mapListById(Collection<V> list) {
		Map<K, V> map = new HashMap<K, V>();
		for (V item : list) {
			map.put(item.getId(), item);
		}
		return map;
	}



}
