package org.coursera.androidcapstone.symptommanagement.common.rest;

import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;

import retrofit.http.Body;
import retrofit.http.POST;

public interface PatientServiceApi {

	/**
	 * Inserts a new patient checkin into the database with associated answers to 
	 * pain, drink/eat and medication questions.
	 * 
	 * @param checkin The patient checkin to insert
	 * @return the inserted {@link PatientCheckin} with new checkin id
	 */
	@POST(RestConstants.PATIENT_CHECKIN_REST_PATH)
	public PatientCheckin addCheckin(@Body PatientCheckin checkin);

}
