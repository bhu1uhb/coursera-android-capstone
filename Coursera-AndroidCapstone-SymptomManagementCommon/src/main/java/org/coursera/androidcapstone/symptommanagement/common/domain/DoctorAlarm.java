package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="PATIENT_ALERT", schema="APP")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DoctorAlarm implements Serializable,Identifiable<Long>, Nameable  {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="alert_id")
	private long alertId;
	@Column(name="patient_id")
	private long patientId;
	@Column(name="checkin_id")
	private long checkinId;
	@Column(name="alert_code")
	private String alertCode;
	@Column(name="dr_notified")
	private char hasDoctorBeenNotified = 'N';
	

	@Transient
	private UserName patientName;
	
	
	public DoctorAlarm() {
	}
	
	@Override
	@Transient
	public Long getId() {
		return alertId;
	}
		
	public long getAlertId() {
		return alertId;
	}
	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public long getCheckinId() {
		return checkinId;
	}

	public void setCheckinId(long checkinId) {
		this.checkinId = checkinId;
	}

	public String getAlertCode() {
		return alertCode;
	}
	public void setAlertCode(String alertCode) {
		this.alertCode = alertCode;
	}
	public char getHasDoctorBeenNotified() {
		return hasDoctorBeenNotified;
	}
	public void setHasDoctorBeenNotified(char hasDoctorBeenNotified) {
		this.hasDoctorBeenNotified = hasDoctorBeenNotified;
	}

	public UserName getPatientName() {
		return patientName;
	}

	public void setPatientName(UserName patientName) {
		this.patientName = patientName;
	}

	@Override
	public CharSequence getName() {
		return new StringBuilder("Alarm ").append(alertId).append(" for patient ").append(patientId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((alertCode == null) ? 0 : alertCode.hashCode());
		result = prime * result + (int) (alertId ^ (alertId >>> 32));
		result = prime * result + (int) (checkinId ^ (checkinId >>> 32));
		result = prime * result + hasDoctorBeenNotified;
		result = prime * result + (int) (patientId ^ (patientId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoctorAlarm other = (DoctorAlarm) obj;
		if (alertCode == null) {
			if (other.alertCode != null)
				return false;
		} else if (!alertCode.equals(other.alertCode))
			return false;
		if (alertId != other.alertId)
			return false;
		if (checkinId != other.checkinId)
			return false;
		if (hasDoctorBeenNotified != other.hasDoctorBeenNotified)
			return false;
		if (patientId != other.patientId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DoctorAlarm [alertId=" + alertId + ", patientId=" + patientId
				+ ", checkinId=" + checkinId + ", alertCode=" + alertCode
				+ ", hasDoctorBeenNotified=" + hasDoctorBeenNotified
				+ ", patientName=" + patientName + "]";
	}


}
