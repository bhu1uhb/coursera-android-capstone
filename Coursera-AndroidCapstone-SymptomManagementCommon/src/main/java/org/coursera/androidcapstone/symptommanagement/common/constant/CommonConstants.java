package org.coursera.androidcapstone.symptommanagement.common.constant;

public class CommonConstants {

	public static final long MILIS_PER_MINUTE = 60 * 1000;//60 secs/min * 1000 milis/secs 
	public static final long MILIS_PER_HOUR = 60 * MILIS_PER_MINUTE;//60 mins/hr * milis/min 
	public static final long MILIS_PER_DAY = 24 * MILIS_PER_HOUR;//24 hr/day * milis/hr 

}
