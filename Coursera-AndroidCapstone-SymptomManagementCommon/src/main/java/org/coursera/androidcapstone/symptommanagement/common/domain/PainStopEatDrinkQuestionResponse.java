package org.coursera.androidcapstone.symptommanagement.common.domain;

import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;

public enum PainStopEatDrinkQuestionResponse implements Identifiable<String> {
	NO("N","No"),
	SOME("S","Some"),
	CANT_EAT("C","I can't eat")
	;
	private final String code;
	private final String displayName;
	
	private PainStopEatDrinkQuestionResponse(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}

	@Override
	@Transient
	public String getId() {
		return code;
	}
	
	public String getCode() {
		return code;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	@Override
	public String toString() {
		return displayName;
	}
	
	public static PainStopEatDrinkQuestionResponse findQuestonResponseByCode(String code) {
		return ApplicationUtils.findEnumById(PainStopEatDrinkQuestionResponse.class, code);
	}
}
