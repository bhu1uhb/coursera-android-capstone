package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="user_details", schema="app")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserName implements Serializable, Identifiable<Long>, Nameable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private long userId;
	@Column(name="user_firstname")
	private String firstName;
	@Column(name="user_lastname")
	private String lastName;

	public UserName() {
	}
	
	public UserName(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String patientFirstName) {
		this.firstName = patientFirstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String patientLastName) {
		this.lastName = patientLastName;
	}

	@Transient
	public String getFullName() {
		return getName().toString();
	}
	
	@Override
	@Transient
	public Long getId() {
		return userId;
	}

	@Override
	@Transient
	public CharSequence getName() {
		return new StringBuilder().append(firstName).append(" ").append(lastName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserName other = (UserName) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return getFullName();
	}
}
