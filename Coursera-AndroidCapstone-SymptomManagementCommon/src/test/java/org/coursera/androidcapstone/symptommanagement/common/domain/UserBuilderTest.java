package org.coursera.androidcapstone.symptommanagement.common.domain;

import static org.junit.Assert.*;

import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserBuilderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBuild_Doctor() {
		User dr = UserBuilder.newBuilder(1L, UserType.DOCTOR.getCode()).firstName("Dr").lastName("Smith").emailAddress("smith@dr.com").build();
		
		assertTrue(dr instanceof Doctor);
		assertEquals("Dr Smith", dr.getFullName());
		assertEquals(Long.valueOf(1L), dr.getUserId());
		
	}

	@Test
	public void testBuild_Patient() {
		Patient pa = (Patient)UserBuilder.newBuilder(2L, UserType.PATIENT.getCode()).firstName("Joe").lastName("Jones").dateOfBirth(ApplicationUtils.convertStringToSqlDate("11/03/1988")).emailAddress("joe@jones.com").build();
		
		assertTrue(pa instanceof Patient);
		assertEquals("Joe Jones", pa.getFullName());
		assertEquals(Long.valueOf(2L), pa.getUserId());
		assertEquals("1988-11-03", pa.getBirthDate().toString());
	}
	
	@Test
	public void testBuildUpon() {
		
		Doctor dr = (Doctor)UserBuilder.newBuilder(1L, UserType.DOCTOR.getCode()).firstName("Dr").lastName("Smith").build();
		
		assertEquals("Dr Smith", dr.getFullName());
		assertEquals(Long.valueOf(1L), dr.getUserId());
		
		Doctor dr2 = (Doctor)UserBuilder.buildUpon(dr).emailAddress("smith@dr.com").build();

		assertEquals("Dr Smith", dr2.getFullName());
		assertEquals(Long.valueOf(1L), dr2.getUserId());
		assertEquals("smith@dr.com", dr2.getEmailAddress());
	}

}
