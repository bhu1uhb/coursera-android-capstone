package org.coursera.androidcapstone.symptommanagement.common.domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PatientTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCompareTo() {
		Patient p1 = new Patient();
		p1.setTotalAlertCount(5);
		
		Patient p2 = new Patient();
		p2.setTotalAlertCount(5);
		
		assertTrue(p1.compareTo(p2) == 0);
		
		p2.setTotalAlertCount(10);
		assertTrue(p1.compareTo(p2) == -1);
		assertTrue(p2.compareTo(p1) == 1);
		
		p2.setTotalAlertCount(3);
		assertTrue(p1.compareTo(p2) == 1);
		assertTrue(p2.compareTo(p1) == -1);
		
		
	}

}
