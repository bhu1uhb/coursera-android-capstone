package org.coursera.androidcapstone.symptommanagement.common.util;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlertCriteria;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagement.common.domain.UserType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class ApplicationUtilsTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindEnumById() {
		assertEquals(DoctorAlertCriteria.CANT_EAT_12, ApplicationUtils.findEnumById(DoctorAlertCriteria.class, "CNT"));
		assertEquals(PainSeverityLevel.MODERATE, ApplicationUtils.findEnumById(PainSeverityLevel.class, "MOD"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindEnumById_BogusValue() {
		ApplicationUtils.findEnumById(PainSeverityLevel.class, "foo");
	}
	
	@Test
	public void testConvertStringToSqlDate() {
		String expected = "2014-10-01";
		String strDate = "10/01/2014";
		Date date = ApplicationUtils.convertStringToSqlDate(strDate);
		assertEquals(expected, date.toString());;
	}

	@Test
	public void testConvertStringToSqlDate_DifferentFormat() {
		String expected = "2014-10-01";
		String strDate = "2014-10-01";
		String format = "yyyy-MM-dd";
		Date date = ApplicationUtils.convertStringToSqlDate(strDate, format);
		assertEquals(expected, date.toString());;
	}

	@Test(expected=IllegalArgumentException.class)
	public void testConvertStringToSqlDate_BadFormat() {
		String strDate = "10/01/2014";
		String format = "qq/yy/xxxx";
		ApplicationUtils.convertStringToSqlDate(strDate, format);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testConvertStringToSqlDate_BadDate() {
		String strDate = "10/44/";
		ApplicationUtils.convertStringToSqlDate(strDate);
	}

	@Test
	public void testConvertStringToSqlTimestamp() {
		String expected = "2014-10-01 01:13:14.0";
		String strDate = "10/01/2014 01:13:14";
		Timestamp date = ApplicationUtils.convertStringToSqlTimestamp(strDate);
		assertEquals(expected, date.toString());;
	}

	@Test
	public void testConvertStringToSqlTimestamp_DifferentFormat() {
		String expected = "2014-10-01 01:13:14.0";
		String strDate = "2014-10-01 01:13:14";
		String format = "yyyy-MM-dd hh:mm:ss";
		Timestamp date = ApplicationUtils.convertStringToSqlTimestamp(strDate, format);
		assertEquals(expected, date.toString());;
	}
	
	@Test
	public void testConvertStringToSqlTimestamp_SymptomManagementQuestionDateTimeFormat() {
		String expected = "2014-11-21 17:44:00.0";
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String strDate = "5:44PM, November 21" + " " + year;
		String format = "hh:mma, MMMM dd yyyy";
		Timestamp date = ApplicationUtils.convertStringToSqlTimestamp(strDate, format);
		assertEquals(expected, date.toString());;
	}

	@Test(expected=IllegalArgumentException.class)
	public void testConvertStringToSqlTimestamp_BadFormat() {
		String strDate = "10/01/2014";
		String format = "qq/yy/xxxx dd:gg:dd";
		ApplicationUtils.convertStringToSqlTimestamp(strDate, format);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testConvertStringToSqlTimestamp_BadDate() {
		String strDate = "10/44/";
		ApplicationUtils.convertStringToSqlTimestamp(strDate);
	}
		
	
	@Test
	public void testIsDifferenceGreaterThanHrs_12Plus1Min_true() {
		int compareHrs = 12;
		Timestamp ts1 = ApplicationUtils.convertStringToSqlTimestamp("11/21/2014 14:30:00.0");
		Timestamp ts2 = ApplicationUtils.convertStringToSqlTimestamp("11/21/2014 02:29:00.0");
		
		boolean actual = ApplicationUtils.isDifferenceGreaterThanHrs(ts1, ts2, compareHrs);
		assertTrue(actual);
	}

	@Test
	public void testIsDifferenceGreaterThanHrs_DifferentDays_true() {
		int compareHrs = 12;
		Timestamp ts1 = ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 22:14:00");
		Timestamp ts2 = ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 02:14:00");
		
		boolean actual = ApplicationUtils.isDifferenceGreaterThanHrs(ts1, ts2, compareHrs);
		assertTrue(actual);
	}

	@Test
	public void testIsDifferenceGreaterThanHrs_12Minus1Min_false() {
		int compareHrs = 12;
		Timestamp ts1 = ApplicationUtils.convertStringToSqlTimestamp("11/21/2014 14:30:00.0");
		Timestamp ts2 = ApplicationUtils.convertStringToSqlTimestamp("11/21/2014 02:31:00.0");
		
		boolean actual = ApplicationUtils.isDifferenceGreaterThanHrs(ts1, ts2, compareHrs);
		assertFalse(actual);
	}

	@Test
	public void testIsDifferenceGreaterThanHrs_equals12_false() {
		int compareHrs = 12;
		Timestamp ts1 = ApplicationUtils.convertStringToSqlTimestamp("11/21/2014 14:30:00.0");
		Timestamp ts2 = ApplicationUtils.convertStringToSqlTimestamp("11/21/2014 02:30:00.0");
		
		boolean actual = ApplicationUtils.isDifferenceGreaterThanHrs(ts1, ts2, compareHrs);
		assertFalse(actual);
	}
	
	
	@Test
	public void testGetNextTimestamp_ThirdInArray() throws Exception {
		Timestamp[] times = {
				new Timestamp(Time.valueOf("11:00:00").getTime()),
				new Timestamp(Time.valueOf("01:00:00").getTime()),
				new Timestamp(Time.valueOf("12:01:00").getTime()),
				new Timestamp(Time.valueOf("11:30:00").getTime())
				};
		Timestamp now = new Timestamp(Time.valueOf("12:00:00").getTime());
		Timestamp actual = ApplicationUtils.getNextTimestamp(times, now);
		
		assertEquals(new Timestamp(Time.valueOf("12:01:00").getTime()), actual);
	}
	
	@Test
	public void testGetMidnightTonightAsLong() throws Exception {
		String strTs = "11/22/2011 00:00:00";
		String expected = "2011-11-23 00:00:00.0";
		Timestamp ts = ApplicationUtils.convertStringToSqlTimestamp(strTs);
		java.util.Date date = new java.util.Date(ts.getTime());
		long lDate = ApplicationUtils.getMidnightTonightAsLong(date);
		assertEquals(expected, new Timestamp(lDate).toString());
		
	}
	

	@Test
	public void testGetMidnightLastNightAsLong() throws Exception {
		String strTs = "11/22/2011 00:00:00";
		String expected = "2011-11-22 00:00:00.0";
		Timestamp ts = ApplicationUtils.convertStringToSqlTimestamp(strTs);
		java.util.Date date = new java.util.Date(ts.getTime());
		long lDate = ApplicationUtils.getMidnightLastNightAsLong(date);
		assertEquals(expected, new Timestamp(lDate).toString());
		
	}
	
	@Test
	@Ignore//FIXME:this test
	public void testGetNextTimestamp_EndOfDay() throws Exception {
		Timestamp[] times = {
				ApplicationUtils.convertStringToSqlTimestamp("11/15/2014 11:00:00"),
				ApplicationUtils.convertStringToSqlTimestamp("11/15/2014 01:00:00"),
				ApplicationUtils.convertStringToSqlTimestamp("11/15/2014 12:01:00"),
				ApplicationUtils.convertStringToSqlTimestamp("11/15/2014 11:30:00")
				};
		Timestamp now = ApplicationUtils.convertSqlTimeToTodaysTimestamp(Time.valueOf("22:00:00"));
		Timestamp actual = ApplicationUtils.getNextTimestamp(times, now);
		Timestamp tomorrow = ApplicationUtils.getSameTimeTomorrow(actual);
		System.out.println("Next Time/Day: " + tomorrow);
		assertEquals(new Timestamp(Time.valueOf("01:00:00").getTime()), actual);
		
	}

	@Test
	public void testConvertTimeStringToSqlTimestamp() throws Exception {
		String time = "6:00:0";
		Calendar date = new GregorianCalendar();		
		Timestamp tomorrow = ApplicationUtils.convertTimeStringToSqlTimestamp(date, time);
		System.out.println("Today: " + tomorrow);
		assertEquals(date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONDAY) + "-" + date.get(Calendar.DAY_OF_MONTH) + " 06:00:00.0", tomorrow.toString());
	}

	
	@Test
	public void testConvertTimeStringToSqlTimestamp_Mutliple() throws Exception {
		String[] times = {
			"12:30:00",
			"8:0:00",
			"19:0:00",
			"16:0:00"
		};
		Timestamp[] timestamps = new Timestamp[4];
		int index = 0;
		for (String time : times) {
			Timestamp ts = ApplicationUtils.convertTimeStringToSqlTimestamp(time);
			timestamps[index] = ts;
			index++;
		}
		System.out.println("Timestamps: " + Arrays.toString(timestamps));
	}
	
	@Test
	public void testGetSameTimeTomorrow() throws Exception {
		Timestamp tsToday = ApplicationUtils.convertStringToSqlTimestamp("10/26/2014 06:00:00");
		Timestamp tomorrow = ApplicationUtils.getSameTimeTomorrow(tsToday);
		System.out.println("Tomorrow: " + tomorrow);
		assertEquals("2014-10-27 06:00:00.0", tomorrow.toString());
	}
	
	@Test
	public void testMapListById() throws Exception {
		IdentifiableString is1 = new IdentifiableString("foo");
		IdentifiableString is2 = new IdentifiableString("bar");
		
		List<IdentifiableString> list = Arrays.asList(is1, is2);
		Map<String, IdentifiableString> map = ApplicationUtils.mapListById(list);
		
		assertEquals(is1, map.get("foo"));
		assertEquals(is2, map.get("bar"));
	}

	private class IdentifiableString implements Identifiable<String> {
		private String item;
		public IdentifiableString(String item) {
			IdentifiableString.this.item = item;
		}
		@Override
		public String getId() {
			return item;
		}
	}

	@Test
	public void testMapListById_Users() throws Exception {
		User p1 = new User(1L, UserType.PATIENT.getCode());
		User p2 = new User(2L, UserType.PATIENT.getCode());
		
		List<User> list = Arrays.asList(p1,p2);
		Map<Long, User> map = ApplicationUtils.mapListById(list);
		
		assertEquals(p1, map.get(1L));
		assertEquals(p2, map.get(2L));
	}
	
	@Test
	public void testMapListById_Patients() throws Exception {
		Patient p1 = new Patient(1L, UserType.PATIENT.getCode());
		Patient p2 = new Patient(2L, UserType.PATIENT.getCode());
		
		List<Patient> list = Arrays.asList(p1,p2);
		Map<Long, Patient> map = ApplicationUtils.mapListById(list);
		
		assertEquals(p1, map.get(1L));
		assertEquals(p2, map.get(2L));
	}
	
	
}
