package org.coursera.androidcapstone.symptommanagement.common.domain;

import static org.junit.Assert.*;

import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PainStopEatDrinkQuestionResponseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDisplayName() {
		assertEquals("Some", PainStopEatDrinkQuestionResponse.SOME.getDisplayName());
	}

	@Test
	public void testToString() {
		assertEquals(PainStopEatDrinkQuestionResponse.SOME.getDisplayName(), PainStopEatDrinkQuestionResponse.SOME.toString());
	}

	@Test
	public void testFindQuestonResponseByCode() {
		assertEquals(PainStopEatDrinkQuestionResponse.CANT_EAT, PainStopEatDrinkQuestionResponse.findQuestonResponseByCode("C"));
		assertEquals(PainStopEatDrinkQuestionResponse.CANT_EAT, PainStopEatDrinkQuestionResponse.findQuestonResponseByCode("c"));
		assertEquals(PainStopEatDrinkQuestionResponse.NO, PainStopEatDrinkQuestionResponse.findQuestonResponseByCode("N"));
		assertEquals(PainStopEatDrinkQuestionResponse.SOME, PainStopEatDrinkQuestionResponse.findQuestonResponseByCode("S"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindQuestonResponseByCode_BogusValue_ThrowsIllegalArgumentException() {
		assertEquals(PainStopEatDrinkQuestionResponse.CANT_EAT, PainStopEatDrinkQuestionResponse.findQuestonResponseByCode("Z"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindQuestonResponseByCode_NullValue_ThrowsIllegalArgumentException() {
		assertEquals(PainStopEatDrinkQuestionResponse.CANT_EAT, PainStopEatDrinkQuestionResponse.findQuestonResponseByCode(null));
	}
}
