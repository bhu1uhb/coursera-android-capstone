package org.coursera.androidcapstone.symptommanagement.common.domain;

import static org.junit.Assert.*;

import org.coursera.androidcapstone.symptommanagement.common.domain.UserType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserTypeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetCode() {
		assertEquals("PA", UserType.PATIENT.getCode());
		assertEquals("DR", UserType.DOCTOR.getCode());
	}

	@Test
	public void testToString() {
		String expected = "Doctor";
		assertEquals(expected, UserType.DOCTOR.toString());
	}

	@Test
	public void testFindUserTypeByCode_Doctor_Ok() throws Exception {
		String typeCode = "DR";
		UserType expected = UserType.DOCTOR;
		assertSame(expected, UserType.findUserTypeByCode(typeCode));
		typeCode = "Dr";
		assertSame(expected, UserType.findUserTypeByCode(typeCode));
	}

	@Test
	public void testFindUserTypeByCode_Patient_Ok() throws Exception {
		String typeCode = "PA";
		UserType expected = UserType.PATIENT;
		assertSame(expected, UserType.findUserTypeByCode(typeCode));
		typeCode = "pa";
		assertSame(expected, UserType.findUserTypeByCode(typeCode));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindUserTypeByCode_BogusCode_ThrowsIllegalArgumentException() throws Exception {
		String typeCode = "foo";
		UserType expected = UserType.DOCTOR;
		assertSame(expected, UserType.findUserTypeByCode(typeCode));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindUserTypeByCode_NullCode_ThrowsIllegalArgumentException() throws Exception {
		String typeCode = null;
		UserType expected = UserType.DOCTOR;
		assertSame(expected, UserType.findUserTypeByCode(typeCode));
	}
}
