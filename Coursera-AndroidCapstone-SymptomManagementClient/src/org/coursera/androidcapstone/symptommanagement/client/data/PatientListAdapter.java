package org.coursera.androidcapstone.symptommanagement.client.data;

import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PatientListAdapter extends ArrayAdapter<Patient> {

	private Patient[] patients;
	private Context context;
	private int layoutResourceId;

	public PatientListAdapter(Context context, int layoutResourceId, Patient[] patients) {
		super(context, layoutResourceId, patients);
		this.context = context;
		this.patients = patients;
		this.layoutResourceId = layoutResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View rowView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			rowView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.patientIdText = (TextView)rowView.findViewById(R.id.patientUserIdText);
			holder.fullNameText = (TextView)rowView.findViewById(R.id.patientFullNameText);
			holder.dateOfBirthText = (TextView)rowView.findViewById(R.id.patientDOBText);
			holder.patientAlertCountText = (TextView)rowView.findViewById(R.id.patientAlertCountText);
		} else {
			holder = (ViewHolder)rowView.getTag();
		}
		
		if (patients.length != 0) {
			Patient patient = patients[position];
			holder.patientIdText.setText(Long.toString(patient.getUserId()));
			holder.fullNameText.setText(patient.getFullName());
			holder.dateOfBirthText.setText(patient.getBirthDate().toString());
			StringBuilder alerts = new StringBuilder().append(patient.getTotalAlertCount()).append("(").append(patient.getActiveAlertCount()).append(")");
			holder.patientAlertCountText.setText(alerts);
		}
		return rowView;
	}

	static class ViewHolder {
		TextView patientIdText;
		TextView fullNameText;
		TextView dateOfBirthText;
		TextView patientAlertCountText;
	}

}
