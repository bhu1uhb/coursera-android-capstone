package org.coursera.androidcapstone.symptommanagement.client.data;

import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CheckinListAdapter extends ArrayAdapter<PatientCheckin> {

	private PatientCheckin[] checkins;
	private Context context;
	private int layoutResourceId;

	public CheckinListAdapter(Context context, int layoutResourceId, PatientCheckin[] checkins) {
		super(context, layoutResourceId, checkins);
		this.context = context;
		this.checkins = checkins;
		this.layoutResourceId = layoutResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View rowView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			rowView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.checkinIdText = (TextView)rowView.findViewById(R.id.checkinListCheckinIdText);
			holder.checkinDateTimeText = (TextView)rowView.findViewById(R.id.checkinListDateTimeText);
			holder.isAlertableText = (TextView)rowView.findViewById(R.id.checkinListAlertText);
		} else {
			holder = (ViewHolder)rowView.getTag();
		}
		
		if (checkins.length != 0) {
			PatientCheckin checkin = checkins[position];
			holder.checkinIdText.setText(Long.toString(checkin.getCheckinId()));
			holder.checkinDateTimeText.setText(checkin.getCheckinDateTime().toString());
			holder.isAlertableText.setText(checkin.isAlertable() ? "*" : "");
		}
		return rowView;
	}

	static class ViewHolder {
		TextView checkinIdText;
		TextView checkinDateTimeText;
		TextView isAlertableText;
	}

}
