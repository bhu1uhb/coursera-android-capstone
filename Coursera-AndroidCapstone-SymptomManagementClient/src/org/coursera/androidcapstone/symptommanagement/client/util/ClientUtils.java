package org.coursera.androidcapstone.symptommanagement.client.util;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.ClientNotificationTimePreferenceKey;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class ClientUtils {
	private static final String TAG = ClientUtils.class.getSimpleName();
	
	public static Timestamp[] getPreferenceTimestamps(Context context) {
		Timestamp[] timestamps = new Timestamp[4];
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);				
		ClientNotificationTimePreferenceKey[] keys = ClientNotificationTimePreferenceKey.values();
		int  index = 0;
		for (ClientNotificationTimePreferenceKey preferenceKey : keys) {
			StringBuilder sb = new StringBuilder();
			String prefKey = preferenceKey.getKey();
			String prefValue = prefs.getString(prefKey, "00:00");
			Log.i(TAG, "Preference: " + prefKey + "=" + prefValue);
			sb.append(Time.valueOf(prefValue + ":00"));
			timestamps[index] = ApplicationUtils.convertTimeStringToSqlTimestamp(sb.toString());
			index++;
		}
		Log.i(TAG, "Preference timestamps: " + Arrays.toString(timestamps));
		return timestamps;
	}
	
}
