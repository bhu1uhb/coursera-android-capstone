package org.coursera.androidcapstone.symptommanagement.client.util;

import java.util.Collection;

//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
/**
 * Manages Spring {@link RestTemplate} invocations.
 * 
 * This class has two options to invoke a Rest call:
 * <ul>
 * <li>{@link #invokeGet()} - returns a single entity result of type T</li>
 * <li>{@link #invokeRestCallForEntityCollection()} - returns a {@link Collection} of entity results of type T</li>
 * </ul>
 * @param <T> the type or {@link Collection} type returned from an invocation
 */
public class RestTemplateManager <T> implements RestClient<T> {

    private final RestTemplate restTemplate = new RestTemplate();
	private final Class<?> entityClass;
    
	/**
	 * Use this for GET requests.
	 * 
	 * @param restUrl the REST URL to invoke containing parameters
	 */
	public RestTemplateManager(Class<?> entityClass) {
		this.entityClass = entityClass;
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	}
	
	/**
	 * Use this for POST requests.
	 * 
	 * @param restUrl the REST URL to invoke containing parameters
	 * @param entity the entity to POST
	 * @param entityClass the {@link Class} of the entity to POST
	 */
	public RestTemplateManager(T entity, Class<?> entityClass) {
		this.entityClass = entityClass;
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	}

	
	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.client.util.RestClient#invokeGetReturnOne(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T invokeGetReturnOne(String restUrl) {
		T result = (T)restTemplate.getForObject(restUrl, entityClass);
		return result;
	}

	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.client.util.RestClient#invokeGetReturnMany(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T[] invokeGetReturnMany(String restUrl) {
		T[] result = (T[])restTemplate.getForObject(restUrl, entityClass);
		return result;
	}
	
	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.client.util.RestClient#invokePost(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T invokePost(T entity, String restUrl) {
        ResponseEntity<T> response = restTemplate.postForEntity(restUrl, entity, (Class<T>)entity.getClass());
	    T result = response.getBody();        
		return result;
	}

}
