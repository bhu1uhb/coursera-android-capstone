package org.coursera.androidcapstone.symptommanagement.client.service;

import java.util.Arrays;

import org.coursera.androidcapstone.symptommanagement.client.activity.PatientListActivity;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.UserPreferenceKey;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.UserType;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Queries the server-side database to check for alarms and
 * present notifications if found.
 * 
 *
 */
public class DoctorAlertNotificationService extends IntentService {
	public static final String TAG = DoctorAlertNotificationService.class.getSimpleName();
	
	public static final int NOTIFICATION_ID = 99;
	
	private String findAlarmsRestUrl = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_ALARMS_ENDPOINT_PATH;
	private final static String updateAlarmRestUrl = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_ALARM_ENDPOINT_PATH;
	
	private static final CharSequence tickerText = "One or more patient alarms available!";
	private static final CharSequence contentTitle = "Patient alarms";
	private static final CharSequence contentText = "Click to respond to patient alarms";
	
	private RestTemplateManager<DoctorAlarm> restTemplateManager = new RestTemplateManager<DoctorAlarm>(DoctorAlarm[].class);

	
	public DoctorAlertNotificationService() {
		super(TAG);
	}
	
	public DoctorAlertNotificationService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i(TAG, "onHandleIntent() started");
		//Get doctorId from preferences and append it to URL 
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String userTypeCode = prefs.getString(UserPreferenceKey.USER_TYPE_CODE.getKey(), UserType.PATIENT.getCode());
		Log.i(TAG, "User type found: " + userTypeCode);
		//Only find alarms for doctors
		if (UserType.findUserTypeByCode(userTypeCode) == UserType.DOCTOR) {
			//Find alarms
			long doctorId = prefs.getLong(UserPreferenceKey.USER_ID.getKey(), 0L);
			DoctorAlarm[] alarms = getAlarms(findAlarmsRestUrl + "/" + doctorId);
			sendNotifications(alarms);
		}
		
	}
	
	
	private DoctorAlarm[] getAlarms(String restUrl) {
		restTemplateManager = new RestTemplateManager<DoctorAlarm>(DoctorAlarm[].class);
		Log.i(TAG, "Rest Url: " + restUrl);
		
		DoctorAlarm[] alarms = restTemplateManager.invokeGetReturnMany(restUrl);
		Log.i(TAG, "Doctor alarms found: " + Arrays.toString(alarms));
		//avoid upstream NPE
		if (alarms == null) {
			alarms = new DoctorAlarm[0];
		}
		return alarms;
	}

	private void sendNotifications(DoctorAlarm[] alarms) {
		if(alarms.length > 0) {
			// The Intent to be used when the user clicks on the Notification View
			Intent notificationIntent = new Intent(this, PatientListActivity.class);
			notificationIntent.putExtra(IntentExtraKey.ALARMS.getKey(), alarms);
	
			// The PendingIntent that wraps the underlying Intent
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					notificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
	
			// Build the Notification
			Notification.Builder notificationBuilder = new Notification.Builder(
					this).setTicker(tickerText)
					.setSmallIcon(android.R.drawable.checkbox_on_background)
					.setAutoCancel(true).setContentTitle(contentTitle)
					.setContentText(contentText).setContentIntent(contentIntent)
					.setSound(ClientConstants.NOTIFICATION_SOUND_URI);
	
			// Get the NotificationManager
			NotificationManager mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
	
			// Pass the Notification to the NotificationManager:
			mNotificationManager.notify(NOTIFICATION_ID,
					notificationBuilder.build());

		}
	}

	/**
	 * Set all hasDoctorBeenNotified records to 'Y'
	 * 
	 * @param alarms
	 */
	private void cancelAlarms(DoctorAlarm[] alarms) {
		restTemplateManager = new RestTemplateManager<DoctorAlarm>(DoctorAlarm.class);
		for (DoctorAlarm alarm : alarms) {
			alarm.setHasDoctorBeenNotified('Y');
			restTemplateManager.invokePost(alarm, updateAlarmRestUrl);
			
		}
	}
}