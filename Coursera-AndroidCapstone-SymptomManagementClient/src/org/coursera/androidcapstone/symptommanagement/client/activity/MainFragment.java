package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class MainFragment extends Fragment implements OnClickListener {
	
	private String medDialogTag = "MedDialog";
	private Medication selectedMedication;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		Button loginButton = (Button)rootView.findViewById(R.id.loginLinkButton);
		loginButton.setOnClickListener(this);
		Button checkinButton = (Button)rootView.findViewById(R.id.checkinLinkButton);
		checkinButton.setOnClickListener(this);
		Button animationButton = (Button)rootView.findViewById(R.id.animationLinkButton);
		animationButton.setOnClickListener(this);
		Button patientListButton = (Button)rootView.findViewById(R.id.patientListLinkButton);
		patientListButton.setOnClickListener(this);

		Button alarmButton = (Button)rootView.findViewById(R.id.alarmPrototypeLinkButton);
		alarmButton.setOnClickListener(this);
		Button medDialogButton = (Button)rootView.findViewById(R.id.dialogMedicationLinkButton);
		medDialogButton.setOnClickListener(this);
		
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.loginLinkButton:
				startActivity(new Intent(getActivity(), LoginActivity.class));			
				break;
			case R.id.checkinLinkButton:
				startActivity(new Intent(getActivity(), CheckinActivity.class));			
				break;			
			case R.id.patientListLinkButton:
				startActivity(new Intent(getActivity(), PatientListActivity.class));			
				break;			
			case R.id.animationLinkButton:
				startActivity(new Intent(getActivity(), HomeScreenActivity.class));			
				break;
			case R.id.alarmPrototypeLinkButton:
				startActivity(new Intent(getActivity(), AlarmDemosActivity.class));			
				break;
			case R.id.dialogMedicationLinkButton:
				new MedicationDialogFragment(this).show(getFragmentManager(), medDialogTag);
				break;
			default:
				break;
		}
	}
	
	public void setMedicationSelection(Medication medication) {
		this.selectedMedication = medication;
		Toast.makeText(getActivity(), "Medication selected: " + selectedMedication, Toast.LENGTH_LONG).show();		
	}


}
