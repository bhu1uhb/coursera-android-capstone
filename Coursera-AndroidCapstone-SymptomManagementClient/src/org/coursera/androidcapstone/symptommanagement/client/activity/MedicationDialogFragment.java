package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;
import org.springframework.web.client.RestTemplate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

@Deprecated //Not used, but I want to save this for future use.
public class MedicationDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {	
	private static final String TAG = MedicationDialogFragment.class.getSimpleName();
	
	private View form; 
	private ListView listView;
	private Medication[] medications;
	private MainFragment parent;
	
	public MedicationDialogFragment(MainFragment parent) {
		this.parent = parent;
	}
	
	@Override
	  public Dialog onCreateDialog(Bundle savedInstanceState) {
	    form = getActivity().getLayoutInflater()
	           .inflate(R.layout.fragment_medication_dialog, null);
	    AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
	    listView = (ListView)form.findViewById(R.id.dialogSelectMedicationListView);
	    
	    new MedicationDialogListRestTask().execute();
	    
	    return(builder.setTitle("Select Medication").setView(form)
	                  .setPositiveButton(android.R.string.ok, this)
	                  .setNegativeButton(android.R.string.cancel, null).create());
	  }

	@Override
	public void onClick(DialogInterface dialog, int which) {
		int position = listView.getCheckedItemPosition();
		Log.i(TAG, "Medication position selected: " + position);
		Medication selectedMedication = medications[position];
		
		parent.setMedicationSelection(selectedMedication);
		
	}
	
	public ListView getListView() {
		return listView;
	}
	
	public class MedicationDialogListRestTask extends AsyncTask<Long, Void, Medication[]> {

		
		@Override
		protected void onPostExecute(Medication[] result) {
			medications = result;
			String[] meds = null;
			meds = new String[medications.length];
			int count = 0;
			for (Medication med : result) {
				meds[count] = med.getMedicationName();
				count++;
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_single_choice, meds);
			listView.setAdapter(adapter);
			listView.setItemChecked(1, true);
		}
		
		@Override
		protected Medication[] doInBackground(Long... params) {
			
	        final String url = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_MEDICATIONS_ENDPOINT_PATH;
	        Log.i(TAG, "REST url: " + url);
	        RestTemplate restTemplate = new RestTemplate(); 
	        medications = restTemplate.getForObject(url, Medication[].class);	        
			return medications;
		}
	}
	
}
