package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.UserPreferenceKey;
import org.coursera.androidcapstone.symptommanagement.client.data.PatientListAdapter;
import org.coursera.androidcapstone.symptommanagement.client.task.DoctorPatientsTask;
import org.coursera.androidcapstone.symptommanagement.client.task.PatientSearchTask;
import org.coursera.androidcapstone.symptommanagement.client.task.UserDetailsTask;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PatientListFragment extends Fragment implements OnItemClickListener, OnClickListener {

	public static final String TAG = PatientListFragment.class.getSimpleName();
	private ListView patientListView;
	private TextView patientNameView;
	private EditText patientSearchTermEditText;
	long doctorId;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "PatientListFragment.onCreateView() called....");
		Bundle extras = getActivity().getIntent().getExtras();
		Patient patient = null;
		if (extras != null) {
			patient = (Patient)extras.get(IntentExtraKey.PATIENT.name());
		}
		
		
		View rootView = inflater.inflate(R.layout.fragment_patient_list,
				container, false);
		patientListView = (ListView)rootView.findViewById(R.id.patientListView);
		patientListView.setOnItemClickListener(this);
		patientNameView = (TextView)rootView.findViewById(R.id.doctorFullnameText);

		Button searchButton = (Button)rootView.findViewById(R.id.patientSearchButton);
		searchButton.setOnClickListener(this);
		patientSearchTermEditText = (EditText)rootView.findViewById(R.id.patientSearchEditText);
		
		//Pull doctorId from preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		doctorId = prefs.getLong(UserPreferenceKey.USER_ID.getKey(), 0L);
		
		if (patient == null) {
			new DoctorPatientsTask(getActivity(), patientListView).execute(doctorId);
		} else {
			Patient[] patients = {patient};
			PatientListAdapter adapter = new PatientListAdapter(getActivity(), R.layout.patient_list_item, patients);
			patientListView.setAdapter(adapter);
		}
		
		new FindDoctorRestTask().execute(doctorId);
		return rootView;
	}

	
	private class FindDoctorRestTask extends UserDetailsTask {

		@Override
		protected void onPostExecute(User result) {
			Log.i(TAG, "User found: " + result);
			
			StringBuilder sb = new StringBuilder("Doctor: ").append(result.getFullName()).append(" (ID=").append(result.getUserId()).append(")");
			patientNameView.setText(sb);
		}
		
	}
	

	@Override
	@SuppressWarnings("unchecked") //Adapter view is a PatientListAdapter, a subclass of ArrayAdapter 
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ArrayAdapter<Patient> adapter = (ArrayAdapter<Patient>)parent.getAdapter();
		Patient patient = adapter.getItem(position);
		
		//Open PatientDetailsActivity, passing patientId
		Intent intent = new Intent(getActivity(), PatientCheckinsListActivity.class);
		intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
		startActivity(intent);			
		
	}


	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.patientSearchButton) {
			//Call PatientSearchTask to do the search and forward back to this activity
			String patientSearchfullName = patientSearchTermEditText.getText().toString();
			if (patientSearchfullName == null || patientSearchfullName.equals("")) {
				Toast.makeText(getActivity(), "Please enter a full name to search", Toast.LENGTH_LONG).show();
			} else {
				new PatientSearchTask(getActivity()).execute(Long.toString(doctorId), patientSearchfullName);
			}
		}
	}



	
}
