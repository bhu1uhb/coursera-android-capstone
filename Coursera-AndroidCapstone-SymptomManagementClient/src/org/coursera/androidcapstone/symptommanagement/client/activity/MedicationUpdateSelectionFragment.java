package org.coursera.androidcapstone.symptommanagement.client.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.task.PatientMedicationSaveTask;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MedicationUpdateSelectionFragment extends Fragment implements OnClickListener {

	private static final String TAG = MedicationUpdateSelectionFragment.class.getSimpleName();
	
	private Medication[] medications;
	private ListView listView;
	private Patient patient;
	private Medication[] oldMedications;//from previous page
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Bundle extras = getActivity().getIntent().getExtras();
		patient = (Patient)extras.get(IntentExtraKey.PATIENT.getKey());
		
		Log.i(TAG, "Medication selected on Update Medication screen: " + Arrays.toString(oldMedications));
		
		View rootView = inflater.inflate(R.layout.fragment_medication_selection_update, container,
				false);
		
		listView = (ListView)rootView.findViewById(R.id.selectMedicationListView);
		
		TextView patientView = (TextView)rootView.findViewById(R.id.selectMedicationPatientFullnameText);
		patientView.setText(new StringBuilder("Patient: ").append(patient.getFullName()));
		Button selectMedicationSaveButton = (Button)rootView.findViewById(R.id.selectMedicationSaveButton);
		selectMedicationSaveButton.setOnClickListener(this);
		Button selectMedicationCancelButton = (Button)rootView.findViewById(R.id.selectMedicationCancelButton);
		selectMedicationCancelButton.setOnClickListener(this);
		
		new MedicationListRestTask().execute();
		
		return rootView;
	}
	
	
	public class MedicationListRestTask extends AsyncTask<Long, Void, Medication[]> {

		
		@Override
		protected void onPostExecute(Medication[] result) {
			medications = result;
			Log.i(TAG, "Medications to show for updating: " + Arrays.toString(medications));
			String[] meds = null;
			meds = new String[medications.length];
			int index = 0;
			for (Medication med: medications) {
				meds[index] = med.getMedicationName();
				index++;
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, meds);
			listView.setAdapter(adapter);
			int position = 0;
			//Select old medications
			for (Medication medication: medications) {
				for(Medication oldMedication : oldMedications) {
					long oldMedId = oldMedication.getMedicationId(); 
					if (oldMedId != 0 && oldMedId == medication.getMedicationId()) {
						listView.setItemChecked(position, true);
						Log.i(TAG, "Position checked: " + position);
					}
				}
				position++;
			}
		}
		
		
		@Override
		protected Medication[] doInBackground(Long... params) {
			RestTemplateManager<Medication> templateManager = new RestTemplateManager<Medication>(Medication[].class);
			//Find All medications
	        final String allMedUrl = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_MEDICATIONS_ENDPOINT_PATH;
	        Log.i(TAG, "All Med REST url: " + allMedUrl);
			medications = templateManager.invokeGetReturnMany(allMedUrl);
			
			//FInd patientMedications
	        final String patMedUrl = ClientConstants.REST_HOST_URL + 
	        		RestConstants.DOCTOR_FIND_PATIENT_MEDICATIONS_ENDPOINT_PATH + "/" + patient.getUserId();
	        Log.i(TAG, "Patient Med REST url: " + patMedUrl);
			
			oldMedications = templateManager.invokeGetReturnMany(patMedUrl);
			
			return medications;
		}
		
	}


	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.selectMedicationSaveButton:
				//find checked items
				List<Medication> newMedications = new ArrayList<Medication>();
				int len = listView.getCount();
				SparseBooleanArray checked = listView.getCheckedItemPositions();
				for (int i = 0; i < len; i++)
					if (checked.get(i)) {
						newMedications.add(medications[i]);
					}				
				long patientId = patient.getUserId();
				//Save patient meds
				new PatientMedicationSaveTask(getActivity(), patient, oldMedications, newMedications.toArray(new Medication[]{}))
					.execute(patientId);
				break;
			
			case R.id.selectMedicationCancelButton:
				//FIXME: Remove button
//				Intent intent = new Intent(getActivity(), PatientMedicationMaintenanceActivity.class);
//				intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
//				intent.putExtra(IntentExtraKey.ACTION.getKey(), Action.CANCEL);
//				
//				startActivity(intent);				
				break;
			default:
				break;
		}
	}
}
