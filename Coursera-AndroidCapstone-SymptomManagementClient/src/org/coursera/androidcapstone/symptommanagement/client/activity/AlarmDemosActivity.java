package org.coursera.androidcapstone.symptommanagement.client.activity;

import java.sql.Time;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.ClientNotificationTimePreferenceKey;
import org.coursera.androidcapstone.symptommanagement.client.receiver.CheckinNotificationReceiver;
import org.coursera.androidcapstone.symptommanagement.client.receiver.DoctorAlertNotificationReceiver;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AlarmDemosActivity extends Activity implements OnClickListener {

	private AlarmManager mAlarmManager;
	private static final long INITIAL_ALARM_DELAY = 1 * 60 * 1000L;//n*60*1000L = n minutes
	protected static final long JITTER = 5000L;
	
	protected long wakeUpTime; 
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm_demos);
		// Get the AlarmManager Service
		mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		
		final Button repeatingAlarmButton = (Button) findViewById(R.id.repeating_alarm_button);
		repeatingAlarmButton.setOnClickListener(this);
		
		final Button cancelRepeatingAlarmButton = (Button) findViewById(R.id.cancel_repeating_alarm_button);
		cancelRepeatingAlarmButton.setOnClickListener(this);
		
		final Button doctorAlertButton = (Button)findViewById(R.id.doctor_alert_alarm_button);
		doctorAlertButton.setOnClickListener(this);
		
		//FIXME: remove hard coding by getting value from Preferences
		wakeUpTime = SystemClock.elapsedRealtime() + INITIAL_ALARM_DELAY;
		
		//show preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);				
		ClientNotificationTimePreferenceKey[] keys = ClientNotificationTimePreferenceKey.values();
		StringBuilder sb = new StringBuilder();
		for (ClientNotificationTimePreferenceKey preferenceKey : keys) {
			String time = prefs.getString(preferenceKey.getKey(),"00:00");
			sb.append(Time.valueOf(time + ":00"));
			sb.append('\n');
		}
		TextView prefView = (TextView)findViewById(R.id.notificationPrototypeSettings);
		prefView.setText(sb);
	}

	private PendingIntent getBroadcastPendingIntent(Class<? extends BroadcastReceiver> brClass) {
		// Create an Intent to broadcast to the AlarmNotificationReceiver
		Intent notificationReceiverIntent = new Intent(this, brClass);
		// Create an PendingIntent that holds the NotificationReceiverIntent
		PendingIntent notificationReceiverPendingIntent = PendingIntent.getBroadcast(this, 0, notificationReceiverIntent, 0);
		return notificationReceiverPendingIntent;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		PendingIntent pendingIntent = null;
		switch (id) {
		case R.id.repeating_alarm_button:
			
			pendingIntent = getBroadcastPendingIntent(CheckinNotificationReceiver.class);
			// Set alarm
			
			//Triggered after wakeUpTime has elapsed.
			//This works for a single alarm
			mAlarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
					INITIAL_ALARM_DELAY,
					pendingIntent);

			//Set multiple alarms
//			setAllAlarms();
			
			// Show Toast message
			Toast.makeText(getApplicationContext(), "Checkin Alarm Set",
					Toast.LENGTH_LONG).show();
			
			break;
		
		case R.id.cancel_repeating_alarm_button:
			pendingIntent = getBroadcastPendingIntent(CheckinNotificationReceiver.class);		
			cancelAlarm(pendingIntent);
				
			break;
			
		case R.id.doctor_alert_alarm_button:
			pendingIntent = getBroadcastPendingIntent(DoctorAlertNotificationReceiver.class);
			mAlarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
					10000L,//10 seconds
					pendingIntent);
			Toast.makeText(getApplicationContext(), "Doctor Alert invoked",
					Toast.LENGTH_LONG).show();
		
		default:
			break;
		}
	}

			
	private void cancelAlarm(PendingIntent pendingIntent) {
		// Cancel all alarms using mNotificationReceiverPendingIntent
		mAlarmManager.cancel(pendingIntent);
		
		Toast.makeText(getApplicationContext(), "Repeating Alarm Cancelled",
				Toast.LENGTH_LONG).show();		
	}
}
