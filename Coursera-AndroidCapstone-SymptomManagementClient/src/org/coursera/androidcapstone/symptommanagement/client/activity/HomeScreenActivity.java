package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HomeScreenActivity extends Activity implements OnClickListener {

	public static final String TAG = HomeScreenActivity.class.getSimpleName();
	private Button loginButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_screen);
		loginButton = (Button)findViewById(R.id.animatedloginLinkButton);
		loginButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.action_alarm_demos:
			startActivity(new Intent(this, AlarmDemosActivity.class));
			break;

		case R.id.action_main_activity:
			startActivity(new Intent(this, MainActivity.class));
			break;

		default:
			break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.animatedloginLinkButton:
			startActivity(new Intent(this, LoginActivity.class));
			break;
		default:
			Log.e(TAG, "View " + v + " is not handled in onClick");
			//FIXME: add Toast
			break;
		}
	}
}
