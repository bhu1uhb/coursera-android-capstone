package org.coursera.androidcapstone.symptommanagement.client.activity;

import java.util.List;

import org.coursera.androidcapstone.symptommanagementclient.R;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

/**
 * A {@link PreferenceActivity} that presents a set of application settings.
 * 
 */
public class SettingsActivity extends PreferenceActivity {
	private boolean needResource=false;

	  @SuppressWarnings("deprecation")
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	    if (needResource) {
	      addPreferencesFromResource(R.xml.preferences);
	    }
	  }

	  @Override
	  public void onBuildHeaders(List<Header> target) {
	    if (onIsHidingHeaders() || !onIsMultiPane()) {
	      needResource=true;
	    }
	    else {
	      loadHeadersFromResource(R.xml.pref_headers, target);
	    }
	  }

	  @Override
	  protected boolean isValidFragment(String fragmentName) {
	    return(NotificationPreferenceFragment.class.getName().equals(fragmentName));
	  }
	  
	/**
	 * This fragment shows notification preferences. 
	 */
	public static class NotificationPreferenceFragment extends
			PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_notification);

		}
	}


}
