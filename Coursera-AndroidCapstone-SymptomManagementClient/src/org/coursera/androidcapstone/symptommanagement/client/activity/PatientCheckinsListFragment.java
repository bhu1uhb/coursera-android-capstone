package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.task.PatientCheckinsTask;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class PatientCheckinsListFragment extends Fragment implements OnItemClickListener, OnClickListener {
	
	private static final String TAG = PatientCheckinsListFragment.class.getSimpleName();
	
	private ListView listView;
	
	private Patient patient;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle extras = getActivity().getIntent().getExtras();
		patient = (Patient)extras.get(IntentExtraKey.PATIENT.name());

		View rootView = inflater.inflate(R.layout.fragment_patient_checkins_list,
				container, false);
		listView =  (ListView)rootView.findViewById(R.id.patientCheckinListView);
		listView.setOnItemClickListener(this);
		TextView patientText = (TextView)rootView.findViewById(R.id.patientCheckinNameLabel); 
		patientText.setText(new StringBuilder("Patient: ").append(patient.getFullName()).append(" DOB: ").append(patient.getBirthDate()));
		new PatientCheckinsTask(getActivity(), listView).execute(patient.getUserId());
		Button updateMedsButton = (Button)rootView.findViewById(R.id.updatePatientMedicationsButton);
		updateMedsButton.setOnClickListener(this);
	
		return rootView;
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ArrayAdapter<PatientCheckin> adapter = (ArrayAdapter<PatientCheckin>)parent.getAdapter();
		PatientCheckin checkin = adapter.getItem(position);
		Log.i(TAG, "ListView: " + listView);
		Log.i(TAG, "View parameter to onItemClick(): " + view);
		
		
		//Open PatientDetailsActivity, passing patient and checkin
		Intent intent = new Intent(getActivity(), PatientDetailsActivity.class);
		intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
		intent.putExtra(IntentExtraKey.CHECKIN.getKey(), checkin);
		startActivity(intent);			
		
		// Show Toast message
		
	}



	@Override
	public void onClick(View v) {
		Intent intent = new Intent(getActivity(), MedicationUpdateSelectionActivity.class);
		intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
		startActivity(intent);					
	}

}
