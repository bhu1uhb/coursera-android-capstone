package org.coursera.androidcapstone.symptommanagement.client.activity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.UserPreferenceKey;
import org.coursera.androidcapstone.symptommanagement.client.task.CheckinSaveTask;
import org.coursera.androidcapstone.symptommanagement.client.task.PatientMedicationsTask;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class CheckinFragment extends Fragment implements OnClickListener {

	private static final String TAG = CheckinFragment.class.getSimpleName();
	
	private Spinner mouthPainSpinner;
	private Spinner stopEatDrinkSpinner;
	private ListView medQuestionsListView;
	private Button checkinButton;
	private long patientId; 
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_check_in,
				container, false);
		//Pull userId from preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		patientId = prefs.getLong(UserPreferenceKey.USER_ID.getKey(), 0L);
		
		//Mouth pain question
		mouthPainSpinner = (Spinner)rootView.findViewById(R.id.mouthPainSeveritySpinner);
		ArrayAdapter<PainSeverityLevel> aaPainLevels = new ArrayAdapter<PainSeverityLevel>(getActivity(), 
				android.R.layout.simple_list_item_1, PainSeverityLevel.values());
		mouthPainSpinner.setAdapter(aaPainLevels);
		//Eat/drink question
		stopEatDrinkSpinner = (Spinner)rootView.findViewById(R.id.stopEatDrinkSpinner);
		ArrayAdapter<PainStopEatDrinkQuestionResponse> aaStopEatDrink = 
				new ArrayAdapter<PainStopEatDrinkQuestionResponse>(getActivity(), android.R.layout.simple_list_item_1, 
				PainStopEatDrinkQuestionResponse.values());
		stopEatDrinkSpinner.setAdapter(aaStopEatDrink);
		//Take med questions
		medQuestionsListView = (ListView)rootView.findViewById(R.id.didYouTakeMedslistView);
		checkinButton = (Button)rootView.findViewById(R.id.checkinButton);
		checkinButton.setOnClickListener(this);
		
		//Get list of patient medications from server
		new PatientMedicationsTask(getActivity(), medQuestionsListView).execute(patientId);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.checkinButton:
				//Save checkin data
				PatientCheckin checkin = new PatientCheckin();
				checkin.setPatientId(patientId);
				//set checkin date/time to now
				Date now = new Date();
				checkin.setCheckinDateTime(new Timestamp(now.getTime()));
				PainSeverityLevel painLevel = (PainSeverityLevel)mouthPainSpinner.getSelectedItem();
				checkin.setMouthThroatPain(painLevel.getCode());
				PainStopEatDrinkQuestionResponse eatDrinkResponse = (PainStopEatDrinkQuestionResponse)stopEatDrinkSpinner.getSelectedItem();
				checkin.setDoesStopEatDrink(eatDrinkResponse.getCode());
				
				//Add questions and answers
			    List<MedicationQuestion> questions = addQuestionsAndAnswers();				
				checkin.setMedicationQuestions(questions);
				
				Log.i(TAG, "Saving checkin: " + checkin);
				//Do update in an async task
				new CheckinSaveTask(getActivity()).execute(checkin);
				
				//FIXME: Update alarms
				
				break;
	
			default:
				break;
		}
	}

	private List<MedicationQuestion> addQuestionsAndAnswers() {
		List<MedicationQuestion> questions = new ArrayList<MedicationQuestion>();
		//get questions with answers from list view each row at a time
		int count = medQuestionsListView.getCount();
		for(int i=0; i < count; i++) {
			View rowView = medQuestionsListView.getChildAt(i);
			//get values in the row and use to initialize a MedicationQuestion instance.
			TextView idText = (TextView)rowView.findViewById(R.id.medicationItemIdText); 
			CheckBox medicationTakenCheckbox = (CheckBox)rowView.findViewById(R.id.medicationTakenCheckbox);
			TextView medicationItemDateTimeTakenText = (TextView)rowView.findViewById(R.id.medicationItemDateTimeTakenText);
			MedicationQuestion medicationQuestion = new MedicationQuestion();
			medicationQuestion.setMedicationId(Long.valueOf(idText.getText().toString()));
			boolean isChecked = medicationTakenCheckbox.isChecked();
			medicationQuestion.setHasTakenMedication(isChecked ? 'Y' : 'N');
			if (isChecked) {
				String dateTime = medicationItemDateTimeTakenText.getText().toString();
				if (dateTime != null && !dateTime.equals("")) {
					//append year to date
					Calendar today = Calendar.getInstance();
					int year = today.get(Calendar.YEAR);
					dateTime = new StringBuilder(dateTime).append(" ").append(year).toString();
					String format = "hh:mma, MMMM dd yyyy";
					medicationQuestion.setDateTimeMedicineTaken(ApplicationUtils.convertStringToSqlTimestamp(dateTime, format));
				}
			}
			questions.add(medicationQuestion);
		}
		return questions;
	}
	
}
