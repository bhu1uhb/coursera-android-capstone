package org.coursera.androidcapstone.symptommanagement.client.task;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.util.RestClient;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.springframework.web.client.RestTemplate;

import android.os.AsyncTask;
import android.util.Log;

public class UserDetailsTask extends AsyncTask<Long, Void, User> {
	private static final String TAG = UserDetailsTask.class.getSimpleName();	
    private final RestClient<User> restClient = new RestTemplateManager<User>(User.class); 
	
	@Override
	protected User doInBackground(Long... params) {
        final String url = ClientConstants.REST_HOST_URL + RestConstants.USER_ENDPOINT_PATH + "/" + params[0];
        Log.i(TAG, "Rest URL called: " + url);
//        RestTemplate restTemplate = new RestTemplate(); 
//        User user = restTemplate.getForObject(url, User.class);	        
        User user = restClient.invokeGetReturnOne(url);	        
		return user;
	}

}
