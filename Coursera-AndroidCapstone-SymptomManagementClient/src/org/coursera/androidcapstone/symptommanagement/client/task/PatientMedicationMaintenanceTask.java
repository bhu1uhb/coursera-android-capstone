package org.coursera.androidcapstone.symptommanagement.client.task;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.data.MedicationMaintenanceListAdapter;
import org.coursera.androidcapstone.symptommanagement.client.util.RestClient;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

/**
 * Obtains a list of a patient's medication
 * @author craig
 *
 */
public class PatientMedicationMaintenanceTask extends AsyncTask<Long, Void, Medication[]> {
	private static final String TAG = PatientMedicationMaintenanceTask.class.getSimpleName();
	
    private final RestClient<Medication> restClient = new RestTemplateManager<Medication>(Medication[].class);

	private Context context;

	private ListView listView; 

	private Patient patient;
	
    public PatientMedicationMaintenanceTask(Context context, ListView listView, Patient patient) {
    	this.context = context;
    	this.listView = listView;
    	this.patient = patient;
	}
    
    @Override
    protected void onPostExecute(Medication[] results) {
    	MedicationMaintenanceListAdapter adapter = new MedicationMaintenanceListAdapter(context,
    			R.layout.medication_maintenance_item, results, listView, patient);
    	listView.setAdapter(adapter);
    }
    
	@Override
	protected Medication[] doInBackground(Long... params) {
		long patientId= params[0];
        final String url = ClientConstants.REST_HOST_URL + RestConstants.PATIENT_FIND_MEDICATIONS_ENDPOINT + "/" + patientId;
        Log.i(TAG, "Rest URL called: " + url);
        Medication[] patientMedications = restClient.invokeGetReturnMany(url);	        
		return patientMedications;
	}

}
