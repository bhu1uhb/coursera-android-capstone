package org.coursera.androidcapstone.symptommanagement.client.task;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.util.RestClient;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class CheckinSaveTask extends AsyncTask<PatientCheckin, Void, Long> {

	private Context context;
    private final RestClient<PatientCheckin> restClient = new RestTemplateManager<PatientCheckin>(PatientCheckin.class); 
	
	public CheckinSaveTask(Context context) {
		this.context = context;
	}
	
	
	
	
	@Override
	protected void onPostExecute(Long result) {
		Toast.makeText(context, "Checkin saved with new checkinId " + result, Toast.LENGTH_LONG).show();
	}




	@Override
	protected Long doInBackground(PatientCheckin... params) {
		PatientCheckin checkin = params[0];
		//Rest URL
        final String url = ClientConstants.REST_HOST_URL + RestConstants.PATIENT_CHECKIN_REST_PATH;
        
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());		
//        ResponseEntity<PatientCheckin> response = restTemplate.postForEntity(url, checkin, PatientCheckin.class);
//	    PatientCheckin result = response.getBody();        
	    PatientCheckin result = restClient.invokePost(checkin, url);        
	    long newId = result.getCheckinId();
		
		
		return newId;
	}

}
