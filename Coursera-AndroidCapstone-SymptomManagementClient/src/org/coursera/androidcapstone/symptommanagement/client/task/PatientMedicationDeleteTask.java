package org.coursera.androidcapstone.symptommanagement.client.task;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


/**
 * For create, update and logically delete a patient medication.
 * 
 *
 */
public class PatientMedicationDeleteTask extends AsyncTask<Long, Void, PatientMedication> {

	private Context context;
	
	public PatientMedicationDeleteTask(Context context) {
		this.context = context;
	}
	
	
	
	
	@Override
	protected void onPostExecute(PatientMedication result) {
		long patientId = result.getPatientId();
		long medicatioId = result.getMedicationId();
		Toast.makeText(context, "Medication " + medicatioId + " deleted for patient "  + patientId, Toast.LENGTH_LONG).show();
	}




	@Override
	protected PatientMedication doInBackground(Long... params) {
		 //TODO: Logic for create, update and logically delete a patient medication.
		long patientId = 0L;
		long medicationId = 0L;
		if (params.length == 2) {
			patientId = params[0];
			medicationId = params[1];
		}
		//Rest URL
        final String url = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_REMOVE_ASSIGNMED_ENDPOINT_PATH;
        
        PatientMedication patientMed = new PatientMedication();
        patientMed.setPatientId(patientId);
        patientMed.setMedicationId(medicationId);
        patientMed.setIsActive('N');
        
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());		
        ResponseEntity<PatientMedication> response = restTemplate.postForEntity(url, patientMed, PatientMedication.class);
        PatientMedication result = response.getBody();        
		
		return result;
	}

}
