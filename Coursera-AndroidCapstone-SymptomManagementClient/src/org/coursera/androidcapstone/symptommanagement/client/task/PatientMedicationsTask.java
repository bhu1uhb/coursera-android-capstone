package org.coursera.androidcapstone.symptommanagement.client.task;

import java.util.Arrays;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.data.PatientMedicationsListAdapter;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

/**
 * 
 * Obtains a list of a patients medications from the server.
 *
 */
public class PatientMedicationsTask extends AsyncTask<Long, Void, MedicationQuestion[]> {

	private static final String TAG = PatientMedicationsTask.class.getSimpleName();
	
	private Context context;
	private ListView listView;
	
	
	public PatientMedicationsTask(Context context, ListView listView) {
		this.context = context;
		this.listView = listView;
	}

	@Override
	protected void onPostExecute(MedicationQuestion[] result) {
		Log.i(TAG, "Medication Questions found for patient: " + Arrays.toString(result));
		PatientMedicationsListAdapter adapter = new PatientMedicationsListAdapter(context, R.layout.patient_medication_list_item, listView, result);
		listView.setAdapter(adapter);
	}
	
	@Override
	protected MedicationQuestion[] doInBackground(Long... params) {
        final String url = ClientConstants.REST_HOST_URL + RestConstants.PATIENT_FIND_MEDICATIONS_ENDPOINT + "/" + params[0];
        Log.i(TAG, "REST url: " + url);
        RestTemplate restTemplate = new RestTemplate(); 
        Medication[] medications = restTemplate.getForObject(url, Medication[].class);	        
		MedicationQuestion[] questions = new MedicationQuestion[medications.length];
		int index = 0;
		for (Medication medication : medications) {
			MedicationQuestion question = new MedicationQuestion();
			question.setMedicationId(medication.getMedicationId());
			question.setMedication(medication);
			questions[index] = question;
			index++;
		}
		return questions;
	}
}
