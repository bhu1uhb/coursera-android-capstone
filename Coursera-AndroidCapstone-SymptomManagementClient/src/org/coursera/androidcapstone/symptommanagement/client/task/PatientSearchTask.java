package org.coursera.androidcapstone.symptommanagement.client.task;

import org.coursera.androidcapstone.symptommanagement.client.activity.PatientListActivity;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.util.RestClient;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
 
/**
 * {@link AsyncTask} to search for a doctor's patient by full name. Only searches
 * for a single patient is supported, otherwize a {@link Toast} is shown
 * with a not found message. 
 */
public class PatientSearchTask extends AsyncTask<String, Void, Patient> {
	private static final String TAG = PatientSearchTask.class.getSimpleName();	
    
	private final RestClient<Patient> restClient = new RestTemplateManager<Patient>(Patient.class);
	private Context context;

    
    public PatientSearchTask(Context context){
    	this.context = context;
    }
    
    @Override
    protected void onPostExecute(Patient result) {
		Intent intent = new Intent(context, PatientListActivity.class);
		if (result != null) {
			intent.putExtra(IntentExtraKey.PATIENT.getKey(), result);
		} else {
			Toast.makeText(context, "Patient not found. Displaying all patients.", Toast.LENGTH_LONG).show();
		}
    	context.startActivity(intent);
    	
    }
    
	@Override
	protected Patient doInBackground(String... params) {
        final String url = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_PATIENT_ENDPOINT_PATH + "/" + params[0] + "/" + params[1];
        Log.i(TAG, "Rest URL called: " + url);
        Patient user = restClient.invokeGetReturnOne(url);	        
		return user;
	}

}
