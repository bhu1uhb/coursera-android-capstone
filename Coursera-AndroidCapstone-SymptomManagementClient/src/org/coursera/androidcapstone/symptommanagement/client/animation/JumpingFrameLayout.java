package org.coursera.androidcapstone.symptommanagement.client.animation;

import android.content.Context;
import android.util.AttributeSet;
//import android.view.View;
import android.widget.FrameLayout;

/**
 *
 * {@link FrameLayout} subclass that is designed to be held inside another 
 * container and to jumps randomly around its container,
 *
 */
public class JumpingFrameLayout extends FrameLayout {
    public static final int MOVE = 1;

    public MoveImageThread moveImageThread;
    
    public JumpingFrameLayout(Context context) {
        super(context);
        init();
    }

	public JumpingFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init();
    }

    public JumpingFrameLayout(Context context, AttributeSet attrs, int flags) {
        super(context, attrs, flags);
        init();
    }

    private void init() {
		moveImageThread = new MoveImageThread(this);
	}

    @Override
    public void onAttachedToWindow() {
        getHandler().post(moveImageThread);
    }

    
}

