package org.coursera.androidcapstone.symptommanagement.client.animation;

import android.view.View;

/**
 * Thread class to move the {@link JumpingFrameLayout} container
 * randomly around its container.
 *
 */
public class MoveImageThread implements Runnable {

	private final JumpingFrameLayout jumper;

	public MoveImageThread(JumpingFrameLayout jumper) {
		this.jumper = jumper;
	}

	@Override
	public void run() {
		//Get reference to parent frame
        final View parent = (View) jumper.getParent();
        if (parent == null) {
        	return;
        }
        double randX = Math.random();
        double randY = Math.random();
        // reposition in parent using setX() and setY()
        final float width = jumper.getMeasuredWidth();
        final float height = jumper.getMeasuredHeight();
		//Get measurement of parent frame
        final float parentw = parent.getMeasuredWidth();
        final float parenth = parent.getMeasuredHeight();
        moveImage(randX, randY, width, height, parentw, parenth);

        jumper.postDelayed(this, 2000); // let's do this again, soon
	}

	public void moveImage(double randX, double randY, final float width,
			final float height, final float parentw, final float parenth) {
		jumper.setX((float) randX * (parentw - width));
        jumper.setY((float) randY * (parenth - height));
	}
	

	JumpingFrameLayout getJumper() {
		return jumper;
	}
	
}
