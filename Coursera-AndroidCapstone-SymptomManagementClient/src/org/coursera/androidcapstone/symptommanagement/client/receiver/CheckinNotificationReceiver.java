package org.coursera.androidcapstone.symptommanagement.client.receiver;


import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;

import org.coursera.androidcapstone.symptommanagement.client.activity.CheckinActivity;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.ClientNotificationTimePreferenceKey;
import org.coursera.androidcapstone.symptommanagement.client.util.ClientUtils;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class CheckinNotificationReceiver extends BroadcastReceiver {
	// Notification ID to allow for future updates
	private static final int MY_NOTIFICATION_ID = 1;
	private static final String TAG = CheckinNotificationReceiver.class.getSimpleName();

	// Notification Text Elements
	private static final CharSequence tickerText = "Please checkin to Symptom Management!";
	private static final CharSequence contentTitle = "Checkin time";
	private static final CharSequence contentText = "Click to Checkin";

		
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		// The Intent to be used when the user clicks on the Notification View
		Intent notificationIntent = new Intent(context, CheckinActivity.class);

		// The PendingIntent that wraps the underlying Intent
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);

		// Build the Notification
		Notification.Builder notificationBuilder = new Notification.Builder(
				context).setTicker(tickerText)
				.setSmallIcon(android.R.drawable.checkbox_on_background)
				.setAutoCancel(true).setContentTitle(contentTitle)
				.setContentText(contentText).setContentIntent(contentIntent)
				.setSound(ClientConstants.NOTIFICATION_SOUND_URI);

		// Get the NotificationManager
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		// Pass the Notification to the NotificationManager:
		mNotificationManager.notify(MY_NOTIFICATION_ID,
				notificationBuilder.build());

		// Log occurence of notify() call
		Log.i(TAG, "Sending notification at:"
				+ DateFormat.getDateTimeInstance().format(new Date()));
		
		//Set next alarm
		setNextAlarm(context);

	}
	
	
	private void setNextAlarm(Context context) {
		//Get alarm manager for setting next alarm
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		// Create an Intent to broadcast to the AlarmNotificationReceiver
		Intent notificationReceiverIntent = new Intent(context, CheckinNotificationReceiver.class);
		// Create an PendingIntent that holds the NotificationReceiverIntent
		PendingIntent notificationReceiverPendingIntent = PendingIntent.getBroadcast(context, 0, notificationReceiverIntent, 0);
		Timestamp nextCheckinTime = getNextNotificationTime(context);
		Log.i(TAG, "Next checkin time: " + nextCheckinTime);
		Toast.makeText(context, "Next checkin time: " + nextCheckinTime, Toast.LENGTH_LONG).show();
		alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				nextCheckinTime.getTime(),
				notificationReceiverPendingIntent);
		
	}
	
	private Timestamp getNextNotificationTime(Context context) {
		Timestamp[] prefTimes = ClientUtils.getPreferenceTimestamps(context);				
		return ApplicationUtils.getNextTimestamp(prefTimes);
	}
}