package org.coursera.androidcapstone.symptommanagement.client.receiver;

import org.coursera.androidcapstone.symptommanagement.client.service.DoctorAlertNotificationService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Wakes up to notify Doctor of any alerts calling {@link DoctorAlertNotificationService} to
 * do its work in the background. 
 *
 */
public class DoctorAlertNotificationReceiver extends BroadcastReceiver {
	private static final String TAG = DoctorAlertNotificationReceiver.class.getSimpleName();
	
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "onReceive started");
		
		//Start DoctorAlertNotificationService to do query for alerts and notification
		Intent serviceIntent = new Intent(context, DoctorAlertNotificationService.class);
		
		context.startService(serviceIntent);
		
	}

	
}
