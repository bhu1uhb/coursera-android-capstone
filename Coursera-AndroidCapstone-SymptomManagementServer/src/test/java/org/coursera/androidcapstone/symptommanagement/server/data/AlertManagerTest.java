package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.util.Arrays;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckinBuilder;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagement.server.TestConfig;
import org.coursera.androidcapstone.symptommanagement.server.data.AlertManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class AlertManagerTest {

	@Inject
	private AlertManager alertManager;
	
	@Inject
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSaveAlarmForLatestCheckin_checkinsWith12HoursSeverPain() {
		PatientCheckin[] checkins = checkinsWith12HoursSeverPain();		
		DoctorAlarm actual = alertManager.saveAlarmForLatestCheckin(Arrays.asList(checkins));
		
		assertNotNull(actual);
		assertTrue(actual.getPatientId() == 1L);
	}

	@Test
	public void testSaveAlarmForLatestCheckin_checkinsWithAtLeast16HoursOrMoreModerateOrSeverePain() {
		PatientCheckin[] checkins = checkinsWithAtLeast16HoursOrMoreModerateOrSeverePain();		
		DoctorAlarm actual = alertManager.saveAlarmForLatestCheckin(Arrays.asList(checkins));
		
		assertNotNull(actual);
		assertTrue(actual.getPatientId() == 3L);
	}

	@Test
	public void testSaveAlarmForLatestCheckin_checkinsWith12HoursICannotEat() {
		PatientCheckin[] checkins = checkinsWith12HoursICannotEat();		
		DoctorAlarm actual = alertManager.saveAlarmForLatestCheckin(Arrays.asList(checkins));
		
		assertNotNull(actual);
		assertTrue(actual.getPatientId() == 3L);
	}

	@Test
	public void testSaveAlarmForLatestCheckin_EmptyList() {
		//not other checkins persisted except latest one
		PatientCheckin[] checkins = {
				PatientCheckinBuilder.newBuilder(1L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 22:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.mouthThroatPain(PainSeverityLevel.SEVERE.getCode())
				.build()
				
		};		
		DoctorAlarm actual = alertManager.saveAlarmForLatestCheckin(Arrays.asList(checkins));
		
		assertNotNull(actual);
		//alarm contains no data
		assertTrue(actual.getPatientId() == 0L);
	}
	
	@Test
	public void testCheckinWith12HoursSeverePain_Found() {
		PatientCheckin[] checkins = checkinsWith12HoursSeverPain();
		PatientCheckin actual = alertManager.checkinWith12HoursSeverePain(Arrays.asList(checkins));
		
		assertNotNull(actual);
	}

	private PatientCheckin[] checkinsWith12HoursSeverPain() {
		PatientCheckin[] checkins = {
			PatientCheckinBuilder.newBuilder(1L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 22:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.mouthThroatPain(PainSeverityLevel.SEVERE.getCode())
				.build(),
			PatientCheckinBuilder.newBuilder(2L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 02:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.mouthThroatPain(PainSeverityLevel.SEVERE.getCode())
				.build(),
			PatientCheckinBuilder.newBuilder(3L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 06:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
				.build()
		};
		
		return checkins;
	}

	@Test
	public void testCheckinWith12HoursSeverePain_NotFound() {
		PatientCheckin checkin1 = PatientCheckinBuilder.newBuilder(1L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 17:14:00"))
				.mouthThroatPain(PainSeverityLevel.SEVERE.getCode())
				.build();
		PatientCheckin checkin2 = PatientCheckinBuilder.newBuilder(2L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 12:14:00"))
				.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
				.build();
		PatientCheckin checkin3 = PatientCheckinBuilder.newBuilder(3L)
				.patientId(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 06:14:00"))
				.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
				.build();
		
		PatientCheckin actual = alertManager.checkinWith12HoursSeverePain(Arrays.asList(checkin1, checkin2, checkin3));
		
		assertNull(actual);
	}
	
	@Test
	public void testCheckinWithAtLeast16HoursOrMoreModerateOrSeverePain_Found() {
		PatientCheckin[] checkins = checkinsWithAtLeast16HoursOrMoreModerateOrSeverePain();
		PatientCheckin actual = alertManager.checkinWithAtLeast16HoursOrMoreModerateOrSeverePain(Arrays.asList(checkins));

		assertNotNull(actual);
	}

	private PatientCheckin[] checkinsWithAtLeast16HoursOrMoreModerateOrSeverePain() {
		PatientCheckin[] checkins = {
			PatientCheckinBuilder.newBuilder(1L)
				.patientId(3L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 17:14:00"))
				.mouthThroatPain(PainSeverityLevel.SEVERE.getCode())
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.build(),
			PatientCheckinBuilder.newBuilder(2L)
				.patientId(3L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 12:14:00"))
				.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.build(),
			PatientCheckinBuilder.newBuilder(3L)
				.patientId(3L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 06:14:00"))
				.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.build()
		};
		return checkins;
	}
	
	@Test
	public void testCheckinWithAtLeast16HoursOrMoreModerateOrSeverePain_ThirdCheckinMeetsCriteria_Found() {
		PatientCheckin checkin1 = PatientCheckinBuilder.newBuilder(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 17:14:00"))
				.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
				.build();
		PatientCheckin checkin2 = PatientCheckinBuilder.newBuilder(2L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 12:14:00"))
				.mouthThroatPain(PainSeverityLevel.WELL_CONTROLLED.getCode())
				.build();
		PatientCheckin checkin3 = PatientCheckinBuilder.newBuilder(3L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 06:14:00"))
				.mouthThroatPain(PainSeverityLevel.SEVERE.getCode())
				.build();
		
		PatientCheckin actual = alertManager.checkinWithAtLeast16HoursOrMoreModerateOrSeverePain(Arrays.asList(checkin1, checkin2, checkin3));

		assertNotNull(actual);
	}
	

	@Test
	public void testCheckinWith12HoursICannotEat_Found() {
		PatientCheckin[] checkins = checkinsWith12HoursICannotEat();
		PatientCheckin actual = alertManager.checkinWith12HoursICannotEat(Arrays.asList(checkins));

		assertNotNull(actual);
	}

	private PatientCheckin[] checkinsWith12HoursICannotEat() {
		PatientCheckin[] checkins = {
				PatientCheckinBuilder.newBuilder(1L)
						.patientId(3L)
						.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 17:14:00"))
						.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
						.doesStopEatDrink(PainStopEatDrinkQuestionResponse.CANT_EAT.getCode())
						.build(),
				PatientCheckinBuilder.newBuilder(2L)
						.patientId(3L)
						.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 12:14:00"))
						.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
						.doesStopEatDrink(PainStopEatDrinkQuestionResponse.CANT_EAT.getCode())
						.build(),
				PatientCheckinBuilder.newBuilder(3L)
						.patientId(3L)
						.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 06:14:00"))
						.mouthThroatPain(PainSeverityLevel.MODERATE.getCode())
						.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
						.build()
		};
		return checkins;
	}
	
	@Test
	public void testCheckinWith12HoursICannotEat_NotFound() {
		PatientCheckin checkin1 = PatientCheckinBuilder.newBuilder(1L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 17:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.SOME.getCode())
				.build();
		PatientCheckin checkin2 = PatientCheckinBuilder.newBuilder(2L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/22/2104 12:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.CANT_EAT.getCode())
				.build();
		PatientCheckin checkin3 = PatientCheckinBuilder.newBuilder(3L)
				.checkinDateTime(ApplicationUtils.convertStringToSqlTimestamp("10/21/2104 06:14:00"))
				.doesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode())
				.build();
		
		PatientCheckin actual = alertManager.checkinWith12HoursICannotEat(Arrays.asList(checkin1, checkin2, checkin3));

		assertNull(actual);
	}

}
