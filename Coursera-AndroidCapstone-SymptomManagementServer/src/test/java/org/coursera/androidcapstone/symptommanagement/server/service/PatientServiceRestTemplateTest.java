package org.coursera.androidcapstone.symptommanagement.server.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.CheckinQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestionPK;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagement.server.TestConfig;
import org.coursera.androidcapstone.symptommanagement.server.data.CheckinQuestionRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.MedicationQuestionRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.PatientCheckinRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class)
@WebAppConfiguration
@IntegrationTest
public class PatientServiceRestTemplateTest {
	
	@Inject
	private PatientCheckinRepository patientCheckinRepository;
	
	@Inject
	private MedicationQuestionRepository medicationQuestionRepository;
	
	@Inject
	private CheckinQuestionRepository checkinQuestionRepository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddCheckin() {
		final long patientId = 1L;
	    final long medicationId1 = 1L;
	    final long medicationId2 = 3L;
		
		PatientCheckin checkin = new PatientCheckin();
		checkin.setDoesStopEatDrink(PainStopEatDrinkQuestionResponse.NO.getCode());
		checkin.setCheckinDateTime(ApplicationUtils.convertStringToSqlTimestamp("2014-11-12 10:14:22", "yyyy-MM-dd hh:mm:ss"));
		checkin.setMouthThroatPain(PainSeverityLevel.MODERATE.getCode());
		checkin.setPatientId(patientId);
		//Med list with 2 meds
		List<MedicationQuestion> questionList = new ArrayList<MedicationQuestion>();
		//med 1
		MedicationQuestion q1 = new MedicationQuestion();
		q1.setDateTimeMedicineTaken(ApplicationUtils.convertStringToSqlTimestamp("2014-11-12 07:33:15", "yyyy-MM-dd hh:mm:ss"));
		q1.setHasTakenMedication('Y');
		q1.setMedicationId(medicationId1);
		questionList.add(q1);
		//med 2, not taken (null date/time)
		MedicationQuestion q2 = new MedicationQuestion();
		q2.setDateTimeMedicineTaken(null);
		q2.setHasTakenMedication('N');
		q2.setMedicationId(medicationId2);
		questionList.add(q2);		
		checkin.setMedicationQuestions(questionList);
		
		//Rest URL
        final String url = RestConstants.HOST_URL + RestConstants.PATIENT_CHECKIN_REST_PATH;
        
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());		
        ResponseEntity<PatientCheckin> response = restTemplate.postForEntity(url, checkin, PatientCheckin.class);
	    PatientCheckin result = response.getBody();        
	    long newId = result.getCheckinId();
	    
	    assertNotNull(result);
	    assertFalse(result.getCheckinId() == 0L);
	    System.out.println("New CHeckin: " + result);

	    //verify inserted data
	    //checkin questions
	    CheckinQuestion cq = checkinQuestionRepository.findOne(newId);
	    assertNotNull(cq);
	    //med question 1
	    MedicationQuestionPK pk1 = new MedicationQuestionPK(newId, medicationId1);
	    MedicationQuestion mq1 = medicationQuestionRepository.findOne(pk1);
	    assertNotNull(mq1);
	    //med question 2
	    MedicationQuestionPK pk2 = new MedicationQuestionPK(newId, medicationId2);
	    MedicationQuestion mq2 = medicationQuestionRepository.findOne(pk2);
	    assertNotNull(mq2);
	    //master checkin record
	    PatientCheckin newCheckin = patientCheckinRepository.findOne(newId);
	    assertNotNull(newCheckin);
	    
	    List<PatientCheckin> allCheckins = patientCheckinRepository.findCheckinsByPatientId(patientId);
	    System.out.println("All Checkins: " + allCheckins);
	    
	    //delete inserted data
	    checkinQuestionRepository.delete(newId);
		medicationQuestionRepository.delete(pk1);
		medicationQuestionRepository.delete(pk2);
		patientCheckinRepository.delete(newId);
	}

	
	
	@Test
	public void testFindMedicationsByPatientId() throws Exception {
		final long patientId = 1L;
		//Add URL
        final String url = RestConstants.HOST_URL + RestConstants.PATIENT_FIND_MEDICATIONS_ENDPOINT + "/" + patientId;
        
        
        RestTemplate restTemplate = new RestTemplate();
        Medication[] meds = restTemplate.getForObject(url, Medication[].class);
		
        assertNotNull(meds);
        assertTrue(meds.length == 2);
        assertEquals(1L, meds[0].getMedicationId());
        assertEquals(3L, meds[1].getMedicationId());
		
	}
}
