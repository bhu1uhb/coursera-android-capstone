package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages={"org.coursera.androidcapstone.symptommanagement.server.data","org.coursera.androidcapstone.symptommanagement.common.domain"})
@EnableSpringConfigured
@EnableJpaRepositories(basePackages="org.coursera.androidcapstone.symptommanagement.server.data")
public class TestPersistenceConfig {
	 
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] {
				"org.coursera.androidcapstone.symptommanagement.common.domain",
				"org.coursera.androidcapstone.symptommanagement.server.data" });

		//We're using Hibernate as the JPA provider
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		
		em.setJpaProperties(additionalProperties());

		return em;
	}

	/**
	 * Hibernate's optional configuration properties, which includes .
	 * <p> 
	 * See <a href="http://docs.jboss.org/hibernate/core/3.3/reference/en/html/session-configuration.html#configuration-optional">documentaiton</a>
	 * 
	 * @return Hibernate's optional configuration properties
	 */
	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "none");// 'none' is undocumented. It means no hibernate.hbm2ddl.auto value 
		properties.setProperty("hibernate.dialect",
				"org.hibernate.dialect.DerbyDialect");
		return properties;
	}

	/**
	 * Hakari data source for a local network Derby database.
	 * <p>
	 * Gradle configuration:</br>
	 * 	compile 'com.zaxxer:HikariCP-java6:2.1.0'
	 * @return
	 */
	@Bean
	public DataSource dataSource() {

		HikariDataSource ds = new HikariDataSource();
		ds.setMaximumPoolSize(100);
		ds.setDataSourceClassName("org.apache.derby.jdbc.ClientDataSource");
		ds.addDataSourceProperty("serverName", "localhost");
		ds.addDataSourceProperty("databaseName", "symptommgmnt");
		ds.addDataSourceProperty("portNumber", "1527");
		ds.addDataSourceProperty("user", "symptommgmnt");
		ds.addDataSourceProperty("password", "password1");

		return ds;
	}

}
