 package org.coursera.androidcapstone.symptommanagement.server.service;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlertCriteria;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.domain.UserType;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class DoctorServiceRestTemplateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindPatientsSpringRestTemplate() throws Exception {
        final String url = RestConstants.HOST_URL + RestConstants.DOCTOR_PATIENTS_ENDPOINT_PATH + "/2";
        RestTemplate restTemplate = new RestTemplate(); 
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Patient[] patients = restTemplate.getForObject(url, Patient[].class);
		System.out.println("Patients: " + Arrays.toString(patients));
		assertTrue(patients.length == 2);
		int index = 0;
		for (Patient patient : patients) {
			System.out.println("Patient: " + patient);
			assertEquals("Patient", patient.getFirstName());
			assertEquals(UserType.PATIENT, patient.getUserType());
			switch (index) {
				case 0:
					assertEquals("Number1", patient.getLastName());					
					assertEquals("1960-03-21", patient.getBirthDate().toString());
					break;
				case 1:
					assertEquals("Number2", patient.getLastName());
					assertEquals("1954-09-11", patient.getBirthDate().toString());
					break;
				default:
					fail("Should not get here");
					break;
			}
			index++;
		}
		
	}

	@Test
	public void testFindCheckinSpringRestTemplate() throws Exception {
        final String url = RestConstants.HOST_URL + RestConstants.DOCTOR_CHECKIN_ENDPOINT_PATH + "/1";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		PatientCheckin checkin = restTemplate.getForObject(url, PatientCheckin.class);	        
		System.out.println("Checkin: " + checkin);
		assertEquals(1, checkin.getCheckinId());
		assertEquals(1, checkin.getPatientId());
		assertEquals("SEV", checkin.getMouthThroatPain());
	}


	@Test
	public void testFindCheckinsSpringRestTemplate() throws Exception {
        final String url = RestConstants.HOST_URL + RestConstants.DOCTOR_CHECKINS_ENDPOINT_PATH + "/3";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
        PatientCheckin[] checkins = restTemplate.getForObject(url, PatientCheckin[].class);
		System.out.println("Checkins: " + checkins);
		assertTrue(checkins.length == 2);
		int index = 0;
		for (PatientCheckin checkin : checkins) {
			switch (index) {
			case 0:
				assertEquals(2, checkin.getCheckinId());
				assertEquals("WLC", checkin.getMouthThroatPain());
				assertEquals("C", checkin.getDoesStopEatDrink());
				assertEquals("2014-10-18 12:27:44.627", checkin.getCheckinDateTime().toString());
				break;
			case 1:
				assertEquals(3, checkin.getCheckinId());					
				assertEquals("SEV", checkin.getMouthThroatPain());
				assertEquals("C", checkin.getDoesStopEatDrink());
				assertEquals("2014-10-18 09:22:00.0", checkin.getCheckinDateTime().toString());
				break;
			default:
				fail("Should not get here");
				break;
		}
		index++;			
		}
	}
	
	@Test
	public void testFindAlarmsSpringRestTemplate() throws Exception {
		String url = RestConstants.HOST_URL + RestConstants.DOCTOR_ALARMS_ENDPOINT_PATH + "/2" ;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
        DoctorAlarm[] alarms = restTemplate.getForObject(url, DoctorAlarm[].class);
        System.out.println("Alarms: " + alarms);
        assertTrue(alarms.length == 1);
        
	}
	
	@Test
	public void testFindAllMedications() throws Exception {
        final String url = RestConstants.HOST_URL + RestConstants.DOCTOR_MEDICATIONS_ENDPOINT_PATH;
        
        RestTemplate restTemplate = new RestTemplate(); 
        Medication[] medications = restTemplate.getForObject(url, Medication[].class);
        
        assertNotNull(medications);
        assertTrue(medications.length == 3);
        System.out.println("Medications: " + Arrays.toString(medications));
	}
	
	
	@Test
	public void tesUpdatePatientAlarmNotificationFlag() throws Exception {
        final String url = RestConstants.HOST_URL + RestConstants.DOCTOR_ALARM_ENDPOINT_PATH;
		DoctorAlarm alarm = new DoctorAlarm();
		alarm.setAlertId(1L);
		alarm.setCheckinId(1L);
		alarm.setPatientId(1L);
		alarm.setAlertCode(DoctorAlertCriteria.SEVERE_PAIN_12.getCode());
		alarm.setHasDoctorBeenNotified('Y');
		
        RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<DoctorAlarm> response = restTemplate.postForEntity(url, alarm, DoctorAlarm.class);
		DoctorAlarm actual = response.getBody(); 
		
		assertNotNull(actual);
		assertEquals('Y', actual.getHasDoctorBeenNotified());
		
		//rollback this change and verify rollback
		alarm.setHasDoctorBeenNotified('N');
		response = restTemplate.postForEntity(url, alarm, DoctorAlarm.class);
		DoctorAlarm reverted = response.getBody(); 
		
		assertNotNull(reverted);
		assertTrue('N' == reverted.getHasDoctorBeenNotified());
		
		
	}
	
}
