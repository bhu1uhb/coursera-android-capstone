package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class MedicationQuestionRepositoryTest {

	@Inject
	private MedicationQuestionRepository repository;
	
	@Inject
	private PatientCheckinRepository patientCheckinRepository;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindMedicationQuestionByCheckinIdAndMedicationId() {
		long checkinId = 1L;
		long medicationId = 1L;
		
		MedicationQuestion medicationQuestion = repository.findMedicationQuestionByCheckinIdAndMedicationId(checkinId, medicationId);
		assertNotNull(medicationQuestion);
		assertEquals('Y', medicationQuestion.getHasTakenMedication());
		assertEquals("2014-10-18 12:54:51.494", medicationQuestion.getDateTimeMedicineTaken().toString());
	}

	@Test
	public void testFindMedicationQuestionByCheckinIdAndMedicationId_BogusCheckinId() {
		long checkinId = -99L;
		long medicationId = 1L;
		
		MedicationQuestion medicationQuestion = repository.findMedicationQuestionByCheckinIdAndMedicationId(checkinId, medicationId);
		assertNull(medicationQuestion);
	}
	
	@Test
	public void testFindMedicationQuestionsByCheckinId() {
		long checkinId = 1L;
		
		List<MedicationQuestion> medicationQuestions = repository.findMedicationQuestionsByCheckinId(checkinId);
		assertNotNull(medicationQuestions);
		assertEquals('Y', medicationQuestions.get(0).getHasTakenMedication());
		assertEquals("2014-10-18 12:54:51.494", medicationQuestions.get(0).getDateTimeMedicineTaken().toString());
		assertTrue(medicationQuestions.size() == 2);
	}


	@Test
	public void testFindMedicationQuestionsByCheckinId_BogusCheckingId() {
		long checkinId = -99L;
		
		List<MedicationQuestion> medicationQuestions = repository.findMedicationQuestionsByCheckinId(checkinId);
		assertNotNull(medicationQuestions);
		assertTrue(medicationQuestions.isEmpty());
	}
	
	@Test
	public void testInsertMedicationQuestionList() throws Exception {
		long checkinId = insertCheckin();
		//make sure checkin worked
		assertTrue(checkinId != 0);
		long medicationId = 1L;
		List<MedicationQuestion> questionList = new ArrayList<MedicationQuestion>();
		MedicationQuestion mq1 = new MedicationQuestion(checkinId, medicationId);
		mq1.setHasTakenMedication('N');
		questionList.add(mq1);
		
		Iterable<MedicationQuestion> inserted = repository.save(questionList);
		assertNotNull(inserted);
		List<MedicationQuestion> mqList = (List<MedicationQuestion>)inserted;
		assertFalse(mqList.isEmpty());
		System.out.println("Inserted MedicationQuestion: " + mqList.get(0));
		
		//Check to see if the inserted record is in there
		MedicationQuestion medicationQuestion = repository.findMedicationQuestionByCheckinIdAndMedicationId(checkinId, medicationId);
		assertNotNull(medicationQuestion);
	}
	
	
	private long insertCheckin() {
		//copied from PatientCheckinRepositoryTest
		long patientId = 1L;
		String doesStopEatDrink = PainStopEatDrinkQuestionResponse.SOME.getCode();
		
		PatientCheckin checkin = new PatientCheckin();
		checkin.setPatientId(patientId);
		checkin.setDoesStopEatDrink(doesStopEatDrink);
		checkin.setMouthThroatPain(PainSeverityLevel.MODERATE.getCode());
		Date date = ApplicationUtils.convertStringToSqlDate("11/01/2014"); 
		checkin.setCheckinDateTime(new Timestamp(date.getTime()));
		
		PatientCheckin inserted = patientCheckinRepository.save(checkin);
		System.out.println("Inserted checkin: " + inserted);
		return inserted.getCheckinId();
	}
}
