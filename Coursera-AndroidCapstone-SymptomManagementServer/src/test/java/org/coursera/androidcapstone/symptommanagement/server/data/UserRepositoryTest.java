package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.Doctor;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class UserRepositoryTest {

	@Inject
	private UserRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindUserByLoginNamePatient() {
		String loginName = "PATIENT1";
		User user = repository.findUserByLoginName(loginName);
		assertEquals("Patient", user.getFirstName());
		assertEquals("Number1", user.getLastName());
		assertEquals("1960-03-22", user.getBirthDate().toString());
		assertTrue(user instanceof Patient);
		
		System.out.println("User found: " + user);
	}

	@Test
	public void testFindUserByLoginNameDoctor() {
		String loginName = "DOCTOR1";
		User user = repository.findUserByLoginName(loginName);
		assertEquals("Doctor", user.getFirstName());
		assertEquals("Number2", user.getLastName());
		assertNull(user.getBirthDate());
		assertTrue(user instanceof Doctor);
		
		System.out.println("User found: " + user);
	}
	
	
	@Test
	public void testFindUserById() throws Exception {
		long userId = 1L;
		User user = repository.findUserById(userId);
		assertEquals("Patient", user.getFirstName());
		assertEquals("Number1", user.getLastName());
		assertTrue(user instanceof Patient);
		
	}

	@Test
	public void testFindUserById_NotFound() throws Exception {
		long userId = -99L;//Bad id
		User user = repository.findUserById(userId);
		assertNull(user);
		
	}
	
	@Test
	public void testFindUserByLoginNameAndPassword_Found() throws Exception {
		
		String loginName = "doctor1";
		String loginPwd = "password1";
		User user = repository.findUserByLoginNameAndPassword(loginName.toUpperCase(), loginPwd.toUpperCase());

		assertNotNull(user);
		assertTrue(user.getUserId() == 2L);
	}


	@Test
	public void testFindUserByLoginNameAndPassword_NotFound() throws Exception {
		
		String loginName = "foobar";
		String loginPwd = "password1";
		User user = repository.findUserByLoginNameAndPassword(loginName.toUpperCase(), loginPwd.toUpperCase());

		assertNull(user);
	}
}
