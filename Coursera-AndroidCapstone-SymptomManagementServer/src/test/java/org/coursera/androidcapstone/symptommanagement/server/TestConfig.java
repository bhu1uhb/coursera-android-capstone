package org.coursera.androidcapstone.symptommanagement.server;

import org.coursera.androidcapstone.symptommanagement.server.data.AlertManager;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages={"org.coursera.androidcapstone.symptommanagement.server.util", "org.coursera.androidcapstone.symptommanagement.server.data","org.coursera.androidcapstone.symptommanagement.common.domain"})
@EnableSpringConfigured
public class TestConfig {

 
	@Bean
	public AlertManager alertManager() {
		return new AlertManager();
	}
}
