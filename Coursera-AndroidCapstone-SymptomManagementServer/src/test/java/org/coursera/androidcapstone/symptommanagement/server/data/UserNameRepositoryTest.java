package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.UserName;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class UserNameRepositoryTest {

	
	@Inject
	private UserNameRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindOne() {
		
		long id = 1;
		UserName user = repository.findOne(id);
		
		assertNotNull(user);
		assertEquals("Patient Number1", user.getFullName());
		
	}

	@Test
	public void testFindOne_NotFound() {
		
		long id = -1;
		UserName user = repository.findOne(id);
		
		assertNull(user);
		
	}
	
	@Test
	public void testFindAll() {
		
		Iterable<UserName> users = repository.findAll();
		
		assertTrue(users instanceof List);
		List<UserName> userList = (List<UserName>)users;
		assertTrue(userList.size() == 3);
	}

	
	@Test
	public void testFindUserByFirstNameAndLastName() throws Exception {
		
		String firstName = "Doctor";
		String lastName = "Number2";
		
		List<UserName> user = repository.findUserByFirstNameAndLastName(firstName, lastName);
		
		assertFalse(user.isEmpty());
		assertEquals(firstName + " " + lastName, user.get(0).getFullName());
		
	}


	@Test
	public void testFindUserByFirstNameAndLastName_NotFound() throws Exception {
		
		String firstName = "Doctor";
		String lastName = "Foobar";
		
		List<UserName> user = repository.findUserByFirstNameAndLastName(firstName, lastName);
		
		assertTrue(user.isEmpty());
		
	}
}
