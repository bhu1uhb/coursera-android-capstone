package org.coursera.androidcapstone.symptommanagement.server.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class UserServiceRestTemplateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindUserByLoginCredendials() {
		String loginName = "patient1".toUpperCase();
		String loginPwd = "password1".toUpperCase();
		
        final String url = RestConstants.HOST_URL + RestConstants.USER_FIND_BY_CREDENTIALS_ENDPOINT_PATH + "/" + loginName + "/" + loginPwd;
//        System.out.println("Rest URL: " + url);
        RestTemplate restTemplate = new RestTemplate(); 
        User user = restTemplate.getForObject(url, User.class);	        
        
        assertNotNull(user);
        assertTrue(user.getUserId() == 1L);
	}


	@Test
	public void testFindUserByLoginCredendials_NotFound() {
		String loginName = "foobar".toUpperCase();
		String loginPwd = "password1".toUpperCase();
		
        final String url = RestConstants.HOST_URL + RestConstants.USER_FIND_BY_CREDENTIALS_ENDPOINT_PATH + "/" + loginName + "/" + loginPwd;
//        System.out.println("Rest URL: " + url);
        RestTemplate restTemplate = new RestTemplate(); 
        User user = restTemplate.getForObject(url, User.class);	        
        
        assertNull(user);
	}

	
	@Test
	public void testFindUserById_Found() throws Exception {
		long userId = 2;
		
        final String url = RestConstants.HOST_URL + RestConstants.USER_ENDPOINT_PATH + "/" + userId;
        RestTemplate restTemplate = new RestTemplate(); 
        User user = restTemplate.getForObject(url, User.class);	        

        assertNotNull(user);
        assertEquals("Doctor Number2", user.getFullName());
	}
	
	@Test
	public void testFindUserById_NotFound() throws Exception {
		long userId = -1;
		
        final String url = RestConstants.HOST_URL + RestConstants.USER_ENDPOINT_PATH + "/" + userId;
        RestTemplate restTemplate = new RestTemplate(); 
        User user = restTemplate.getForObject(url, User.class);	        

        assertNull(user);
	}
}
