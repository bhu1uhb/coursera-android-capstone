package org.coursera.androidcapstone.symptommanagement.server.util;

import static org.junit.Assert.*;

import org.coursera.androidcapstone.symptommanagement.common.domain.UserName;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StringUtilsTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseUserFirstAndLastName_BothFirstAndLastName() {
		String first = "Patient";
		String last = "Number1";
		String fullName = first + " " + last;
		
		UserName name = StringUtils.parseUserFirstAndLastName(fullName);
		
		assertNotNull(name);
		assertEquals(first, name.getFirstName());
		assertEquals(last, name.getLastName());
	}

	@Test
	public void testParseUserFirstAndLastName_LastNameOnly() {
		String first = "";
		String last = "Number1";
		String fullName = last;
		
		UserName name = StringUtils.parseUserFirstAndLastName(fullName);
		
		assertNotNull(name);
		assertEquals(first, name.getFirstName());
		assertEquals(last, name.getLastName());
	}


	@Test
	public void testParseUserFirstAndLastName_NullFullNameParameter() {
		String fullName = null;
		
		UserName name = StringUtils.parseUserFirstAndLastName(fullName);
		
		assertNotNull(name);
		assertEquals(null, name.getFirstName());
		assertEquals(null, name.getLastName());
	}
}
