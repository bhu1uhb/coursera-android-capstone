package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class PatientMedicationRepositoryTest {

	@Inject
	private PatientMedicationRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave_InsertPatientMedication() {
		long medicationId = 2L;
		long patientId = 1L;
		PatientMedication patientMedication = new PatientMedication();
		patientMedication.setPatientId(patientId);
		patientMedication.setMedicationId(medicationId);
		
		PatientMedication patientMed = repository.save(patientMedication);
		
		assertEquals(medicationId, patientMed.getMedicationId());
		assertEquals(patientId, patientMed.getPatientId());
		assertEquals('Y', patientMed.getIsActive());
	}


	@Test
	public void testSave_UpdatePatientMedication() {
		long medicationId = 2L;
		long patientId = 3L;
		char isActive = 'N';
		
		PatientMedication patientMedication = new PatientMedication(patientId, medicationId);
		patientMedication.setIsActive(isActive);
		
		PatientMedication patientMed = repository.save(patientMedication);
		
		assertEquals(medicationId, patientMed.getMedicationId());
		assertEquals(patientId, patientMed.getPatientId());
		assertEquals(isActive, patientMed.getIsActive());
	}
	
	@Test
	public void testfindByPatientIdAndMedicationId() {
		long medicationId = 2L;
		long patientId = 3L;
		
		PatientMedication patientMed = repository.findByPatientIdAndMedicationId(patientId, medicationId);
		
		assertEquals(medicationId, patientMed.getMedicationId());
		assertEquals(patientId, patientMed.getPatientId());
		assertEquals('Y', patientMed.getIsActive());
	}
	
	@Test
	public void testfindByPatientIdAndMedicationId_bogusParams_ReturnsNull() {
		long medicationId = 99L;
		long patientId = 99L;
		
		PatientMedication patientMed = repository.findByPatientIdAndMedicationId(patientId, medicationId);
		
		assertNull(patientMed);
	}
}
