package org.coursera.androidcapstone.symptommanagement.server;

import org.coursera.androidcapstone.symptommanagement.server.data.AlertManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
//Tell Spring to go and scan our controller package (and all sub packages) to
//find any Controllers or other components that are part of our applciation.
//Any class in this package that is annotated with @Controller is going to be
//automatically discovered and connected to the DispatcherServlet.
@ComponentScan
//Tell Spring to automatically inject any dependencies that are marked in
//our classes with @Autowired
@EnableAutoConfiguration
@Import(PerisistenceConfig.class)
public class ApplicationConfig {


	@Bean
	public AlertManager alertManager() {
		return new AlertManager();
	}
	
	public static void main(String[] args){
		SpringApplication.run(ApplicationConfig.class, args);
	}

}
