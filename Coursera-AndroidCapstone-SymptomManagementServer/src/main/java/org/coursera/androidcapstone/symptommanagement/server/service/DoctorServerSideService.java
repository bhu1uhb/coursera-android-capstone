package org.coursera.androidcapstone.symptommanagement.server.service;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;
import org.coursera.androidcapstone.symptommanagement.common.domain.UserName;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagement.common.rest.SymptomManagementPathVariable;
import org.coursera.androidcapstone.symptommanagement.server.data.DoctorAlarmRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.DoctorPatientRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.MedicationQuestionRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.MedicationRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.PatientAlertCount;
import org.coursera.androidcapstone.symptommanagement.server.data.PatientAlertCountRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.PatientCheckinRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.PatientMedicationRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.UserNameRepository;
import org.coursera.androidcapstone.symptommanagement.server.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

@RestController
public class DoctorServerSideService {

	
	@Inject
	private PatientCheckinRepository patientCheckinRepository;
	
	@Inject
	private MedicationQuestionRepository medicationQuestionRepository;
	
	@Inject
	private DoctorPatientRepository doctorPatientRepository;
	
	@Inject
	private DoctorAlarmRepository doctorAlarmRepository;
	
	@Inject
	private PatientMedicationRepository patientMedicationRepository;
	
	@Inject
	private MedicationRepository medicationRepository;
	
	@Inject
	private UserNameRepository userNameRepository;
	
	@Inject
	private PatientAlertCountRepository patientAlertCountRepository;
	
	@RequestMapping(value=RestConstants.DOCTOR_FIND_PATIENTS_REST_PATH, method=RequestMethod.GET)
	public Collection<Patient> findAllPatients(@PathVariable(SymptomManagementPathVariable.Name.DOCTOR_ID) long doctorId) {
		
		List<Patient> patients = doctorPatientRepository.findAllPatients(doctorId);
		if (patients != null) {
			List<PatientAlertCount> allAlerts = patientAlertCountRepository.findPatientAllAlertCounts();
			List<PatientAlertCount> activeAlerts = patientAlertCountRepository.findPatientActiveAlertCounts();
			
			//add alert counts for display on patient list screen
			for (Patient patient : patients) {
				for (PatientAlertCount alertCount : allAlerts) {
					if (patient.getUserId() == alertCount.getPatientId()) {
						patient.setTotalAlertCount(alertCount.getAlertCount());
					}					
				}
				for (PatientAlertCount alertCount : activeAlerts) {
					if (patient.getUserId() == alertCount.getPatientId()) {
						patient.setActiveAlertCount(alertCount.getAlertCount());
					}				
				}
				
			}
		}
		return patients;
	}	

	@RequestMapping(value=RestConstants.DOCTOR_PATIENT_SEARCH_FULLNAME_REST_PATH, method=RequestMethod.GET)
	public Patient findPatientByFullName(@PathVariable(SymptomManagementPathVariable.Name.DOCTOR_ID) long doctorId, @PathVariable(SymptomManagementPathVariable.Name.USER_FULLNAME) String patientFullName) {
		
		UserName userName = StringUtils.parseUserFirstAndLastName(patientFullName);
		Patient patient = doctorPatientRepository.findPatientByName(doctorId, userName.getFirstName(), userName.getLastName());
		
		//set alert counts
		if (patient != null) {
			long patientId = patient.getUserId();
			PatientAlertCount allAlerts = patientAlertCountRepository.findPatientAllAlertCountsByPatientId(patientId);
			if (allAlerts != null) {
				patient.setTotalAlertCount(allAlerts.getAlertCount());
			}
			PatientAlertCount activeAlerts = patientAlertCountRepository.findPatientActiveAlertCountsByPatientId(patientId);
			if (activeAlerts != null) {
				patient.setActiveAlertCount(activeAlerts.getAlertCount());
			}
		}
		
		return patient;
	}	
	
	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.server.service.DoctorServiceApi#assignMedication(long, long)
	 */
	@RequestMapping(value=RestConstants.DOCTOR_FIND_CHECKINS_REST_PATH, method=RequestMethod.GET)
	public Collection<PatientCheckin> findCheckins(@PathVariable(SymptomManagementPathVariable.Name.PATIENT_ID) long patientId) {
		
		List<PatientCheckin> checkins = patientCheckinRepository.findCheckinsByPatientId(patientId);
		
		//Add in medication questions
		for (PatientCheckin patientCheckin : checkins) {
			List<MedicationQuestion> medQuestions = medicationQuestionRepository.findMedicationQuestionsByCheckinId(patientCheckin.getCheckinId());
			for (MedicationQuestion medicationQuestion : medQuestions) {
				Medication medication = medicationRepository.findOne(medicationQuestion.getMedicationId());
				medicationQuestion.setMedication(medication);
			}
			patientCheckin.setMedicationQuestions(medQuestions);
			
			//See if the checkin is alertable
			DoctorAlarm alarm = doctorAlarmRepository.findByCheckinId(patientCheckin.getCheckinId());
			if (alarm != null) {
				patientCheckin.setAlertable(true);
			} else {
				patientCheckin.setAlertable(false);				
			}
		}
				
		return checkins;
	}	

	/**
	 * Finds a patient's checkin by checkinId
	 * 
	 * @param checkinId The checkinId primary key to the checkin record.
	 * @return
	 */
	@RequestMapping(value=RestConstants.DOCTOR_FIND_CHECKIN_REST_PATH, method=RequestMethod.GET)
	public PatientCheckin findCheckin(@PathVariable(SymptomManagementPathVariable.Name.CHECKIN_ID) long checkinId) {
		PatientCheckin checkin = patientCheckinRepository.findOne(checkinId); 
		List<MedicationQuestion> questionList = medicationQuestionRepository.findMedicationQuestionsByCheckinId(checkinId);
		for (MedicationQuestion medicationQuestion : questionList) {
			Medication medication = medicationRepository.findOne(medicationQuestion.getMedicationId());
			medicationQuestion.setMedication(medication);
		}
		checkin.setMedicationQuestions(questionList);
		return checkin;
	}
	
	
	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.server.service.DoctorServiceApi#assignMedication(long, long)
	 */
	@RequestMapping(value=RestConstants.DOCTOR_ASSIGN_MEDICATION_REST_PATH, method=RequestMethod.POST)
	public PatientMedication assignMedication(@RequestBody PatientMedication patientMedication) {
		
		
		patientMedication.setIsActive('Y');
		
		PatientMedication newPatientMedication = patientMedicationRepository.save(patientMedication);
		
		return newPatientMedication;
	}
	
	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.server.service.DoctorServiceApi#removeAssignedMedication(long, long)
	 */
	@RequestMapping(value=RestConstants.DOCTOR_REMOVE_ASSIGNED_MEDICATION_REST_PATH, method=RequestMethod.POST)
	public PatientMedication removeAssignedMedication(@RequestBody PatientMedication patientMedication) {

		patientMedication.setIsActive('N');
		
		PatientMedication newPatientMedication = patientMedicationRepository.save(patientMedication);
		
		return newPatientMedication;
	}

	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.server.service.DoctorServiceApi#checkAlarms(long)
	 */
	@RequestMapping(value=RestConstants.DOCTOR_CHECK_ALL_PATIENTS_ALARMS_REST_PATH, method=RequestMethod.GET)
	public Collection<DoctorAlarm> findAlarms(@PathVariable(SymptomManagementPathVariable.Name.DOCTOR_ID) long doctorId) {
		List<DoctorAlarm> alarms = doctorAlarmRepository.findPatientAlarms(doctorId);
		System.out.println("Alarms found: " + alarms);
		for (DoctorAlarm doctorAlarm : alarms) {
			long patientId = doctorAlarm.getPatientId();
			UserName user = userNameRepository.findOne(patientId);
			doctorAlarm.setPatientName(user);
		}
		
		return alarms;
	}

	
	@RequestMapping(value=RestConstants.DOCTOR_MEDICATIONS_ENDPOINT_PATH, method=RequestMethod.GET)
	public Collection<Medication> findAllMedications() {
		Iterable<Medication> meds = medicationRepository.findAll();			
		return Lists.newArrayList(meds);
	}
	
	@RequestMapping(value=RestConstants.DOCTOR_FIND_PATIENT_MEDICATIONS_REST_PATH, method=RequestMethod.GET)
	public List<Medication> findMedications(@PathVariable(SymptomManagementPathVariable.Name.PATIENT_ID) long patientId) {
		
		return medicationRepository.findMedicationsByPatient(patientId);
	}
	
	
	@RequestMapping(value=RestConstants.DOCTOR_ALARM_ENDPOINT_PATH, method=RequestMethod.POST)
	public DoctorAlarm updateDoctorAlarm(@RequestBody DoctorAlarm alarm) {
				
		DoctorAlarm updatedAlarm = doctorAlarmRepository.save(alarm);
		
		return updatedAlarm;
	}

}
