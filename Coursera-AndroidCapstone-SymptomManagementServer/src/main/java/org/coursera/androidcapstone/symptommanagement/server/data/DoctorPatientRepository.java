package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorPatientRepository extends CrudRepository<Patient, Long>{

	@Query(value ="select app.user_credentials.user_id, login_name, login_pwd, user_firstname, user_lastname, user_email, user_phone, user_type, user_birthdate from app.user_credentials inner join app.user_details on app.user_credentials.user_id=app.user_details.user_id join app.doctor_patient_relation on app.user_credentials.user_id=app.doctor_patient_relation.patient_id where app.doctor_patient_relation.doctor_id = ?1", nativeQuery = true)	
	public List<Patient> findAllPatients(long doctorId);

	@Query(value ="select app.user_credentials.user_id, login_name, login_pwd, user_firstname, user_lastname, user_email, user_phone, user_type, user_birthdate from app.user_credentials inner join app.user_details on app.user_credentials.user_id=app.user_details.user_id join app.doctor_patient_relation on app.user_credentials.user_id=app.doctor_patient_relation.patient_id where app.doctor_patient_relation.doctor_id = ?1 and upper(user_firstname)=upper(?2) and upper(user_lastname)=upper(?3)", nativeQuery = true)	
	public Patient findPatientByName(long doctorId, String patientFirstName, String patientLastName);
}
