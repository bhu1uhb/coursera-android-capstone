package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientCheckinRepository extends CrudRepository<PatientCheckin, Long>{


	@Query(value="select ci.checkin_id, ci.patient_id, ci.checkin_datetime, cq.mouth_throat_pain, cq.does_stop_eat_drink from app.checkin as ci inner join app.checkin_questions cq on ci.checkin_id=cq.checkin_id where ci.checkin_id=?1 and ci.patient_id=?2", nativeQuery=true)
	public PatientCheckin findByPatientIdAndCheckinId(long patientId, long checkinId);

	@Query(value="select ci.checkin_id, ci.patient_id, ci.checkin_datetime, cq.mouth_throat_pain, cq.does_stop_eat_drink from app.checkin as ci inner join app.checkin_questions cq on ci.checkin_id=cq.checkin_id where ci.patient_id=?1 order by ci.checkin_datetime desc", nativeQuery=true)
	public List<PatientCheckin> findCheckinsByPatientId(long patientId);

}
