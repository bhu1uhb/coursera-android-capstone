package org.coursera.androidcapstone.symptommanagement.server.data;

import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
	
	@Query(value ="select app.user_credentials.user_id, login_name, login_pwd, user_firstname, user_lastname, user_email, user_phone, user_type, user_birthdate from app.user_credentials inner join app.user_details on app.user_credentials.user_id=app.user_details.user_id where login_name = ?1", nativeQuery = true)
	public User findUserByLoginName(String loginName); 

	@Query(value ="select app.user_credentials.user_id, login_name, login_pwd, user_firstname, user_lastname, user_email, user_phone, user_type, user_birthdate from app.user_credentials inner join app.user_details on app.user_credentials.user_id=app.user_details.user_id where login_name = ?1 and login_pwd=?2", nativeQuery = true)
	public User findUserByLoginNameAndPassword(String loginName, String loginPwd); 
	
	@Query(value ="select app.user_credentials.user_id, login_name, login_pwd, user_firstname, user_lastname, user_email, user_phone, user_type, user_birthdate from app.user_credentials inner join app.user_details on app.user_credentials.user_id=app.user_details.user_id where app.user_credentials.user_id = ?1", nativeQuery = true)
	public User findUserById(long userId); 
	
}
