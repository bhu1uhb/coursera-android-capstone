package org.coursera.androidcapstone.symptommanagement.server.service;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagement.common.rest.SymptomManagementPathVariable;
import org.coursera.androidcapstone.symptommanagement.server.data.UserRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserServerSideService {

	@Inject
	private UserRepository userRepository;
	
	
	
	
	@RequestMapping(value=RestConstants.USER_FIND_BY_CREDENTIALS_REST_PATH, method=RequestMethod.GET)
	public User findUserByLoginCredendials(
			@PathVariable(SymptomManagementPathVariable.Name.LOGIN_NAME) String loginName,
			@PathVariable(SymptomManagementPathVariable.Name.LOGIN_PWD) String loginPwd
			) {
		
		String name = loginName != null ? loginName.toUpperCase() : "";
		String password = loginPwd != null ? loginPwd.toUpperCase() : "";
		User user = userRepository.findUserByLoginNameAndPassword(name, password);
		
		return user;
	}

	@RequestMapping(value=RestConstants.USER_FIND_REST_ENDPOINT_PATH, method=RequestMethod.GET)
	public User findUserById(@PathVariable(SymptomManagementPathVariable.Name.USER_ID) long userId) {
		
		User user = userRepository.findUserById(userId);
		
		return user;
	}
}
