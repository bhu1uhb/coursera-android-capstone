package org.coursera.androidcapstone.symptommanagement.server.util;

import org.coursera.androidcapstone.symptommanagement.common.domain.UserName;

public class StringUtils {

	private StringUtils(){
	}
	
	
	/**
	 * Parses first and last name from the parameter assuming they are separated by one or more spaces.
	 * If there is no delimeter between first and last name, assumes that the parameter is the last name only.
	 * 
	 * @param fullName
	 * @return a {@link UserName} object with the first and last name set. If only one name is sent in as fullName, then returns
	 * the {@link UserName} object with lastName == fullName and firstName an empty {@link String}. If fullName is null, 
	 * a non-null empty UserName object is returned.
	 */
	public static UserName parseUserFirstAndLastName(String fullName) {
		UserName userName = new UserName();
		if (fullName == null) {
			return userName;
		}
		String[] names = fullName.trim().split(" ");
		if (names.length == 2) {
			userName.setFirstName(names[0].trim());
			userName.setLastName(names[1].trim());
		} else if (names.length == 1) {
			userName.setFirstName("");
			userName.setLastName(names[0].trim());			
		}
		
		return userName;
	}
}
