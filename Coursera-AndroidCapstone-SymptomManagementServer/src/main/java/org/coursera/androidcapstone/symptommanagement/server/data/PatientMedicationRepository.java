package org.coursera.androidcapstone.symptommanagement.server.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedicationPK;

public interface PatientMedicationRepository extends CrudRepository<PatientMedication, PatientMedicationPK> {
	
	
	@Query(value="select patient_id, medication_id, is_active from  app.patient_medication where patient_id=?1 and medication_id=?2", nativeQuery=true)
	public PatientMedication findByPatientIdAndMedicationId(long patientId, long medicationId);
}
