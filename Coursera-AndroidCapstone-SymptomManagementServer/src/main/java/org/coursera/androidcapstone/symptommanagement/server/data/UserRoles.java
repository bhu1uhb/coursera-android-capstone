package org.coursera.androidcapstone.symptommanagement.server.data;

import org.coursera.androidcapstone.symptommanagement.common.domain.UserType;
//import org.springframework.security.core.GrantedAuthority;

public enum UserRoles {// implements GrantedAuthority {
	PATIENT(UserType.PATIENT),
	DOCTOR(UserType.DOCTOR)
	;
	private final UserType userType;
	
	private UserRoles(UserType userType) {
		this.userType = userType;
	}
	
	public String getAuthority() {
		return name();
	}

	public UserType getUserType() {
		return userType;
	}
	
	public static UserRoles findUserRoleByUserType(UserType userType) {
		UserRoles userRole = null; 
		UserRoles[] userRoles = values();
		for (UserRoles role : userRoles) {
			if (role.getUserType() == userType)  {
				userRole = role;
				break;
			}
		}
		if (userRole == null) {
			throw new IllegalArgumentException("User role cannot be found with the UserType " + userType);
		}
		return userRole;
	}
}
