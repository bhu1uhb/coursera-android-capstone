package org.coursera.androidcapstone.symptommanagement.server.data;


import org.coursera.androidcapstone.symptommanagement.common.domain.CheckinQuestion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckinQuestionRepository extends CrudRepository<CheckinQuestion,  Long>{

}
