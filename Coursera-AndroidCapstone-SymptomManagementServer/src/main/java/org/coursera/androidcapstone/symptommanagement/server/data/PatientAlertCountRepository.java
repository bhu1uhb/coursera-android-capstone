package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PatientAlertCountRepository extends CrudRepository<PatientAlertCount, Long> {

	@Query(value="select app.patient_alert.patient_id, count(*) alert_count from app.patient_alert  where app.patient_alert.dr_notified='N' group by app.patient_alert.patient_id", nativeQuery=true)
	public List<PatientAlertCount> findPatientActiveAlertCounts();


	@Query(value="select app.patient_alert.patient_id, count(*) alert_count from app.patient_alert group by app.patient_alert.patient_id", nativeQuery=true)
	public List<PatientAlertCount> findPatientAllAlertCounts();
	
	@Query(value="select app.patient_alert.patient_id, count(*) alert_count from app.patient_alert  where app.patient_alert.dr_notified='N' and app.patient_alert.patient_id=?1 group by app.patient_alert.patient_id", nativeQuery=true)
	public PatientAlertCount findPatientActiveAlertCountsByPatientId(long patientId);

	@Query(value="select app.patient_alert.patient_id, count(*) alert_count from app.patient_alert where app.patient_alert.patient_id=?1 group by app.patient_alert.patient_id", nativeQuery=true)
	public PatientAlertCount findPatientAllAlertCountsByPatientId(long patientId);
}
