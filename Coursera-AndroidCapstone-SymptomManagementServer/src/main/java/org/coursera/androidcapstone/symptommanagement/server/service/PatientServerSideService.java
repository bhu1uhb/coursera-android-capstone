package org.coursera.androidcapstone.symptommanagement.server.service;

import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.rest.PatientServiceApi;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagement.common.rest.SymptomManagementPathVariable;
import org.coursera.androidcapstone.symptommanagement.server.data.AlertManager;
import org.coursera.androidcapstone.symptommanagement.server.data.MedicationQuestionRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.MedicationRepository;
import org.coursera.androidcapstone.symptommanagement.server.data.PatientCheckinRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PatientServerSideService implements PatientServiceApi {

	@Inject
	private PatientCheckinRepository patientCheckinRepository;
	
	@Inject
	private MedicationQuestionRepository medicationQuestionRepository;
	
	@Inject
	private MedicationRepository medicationRepository;
	
	@Inject
	AlertManager alertManager;
	
	
	/* (non-Javadoc)
	 * @see org.coursera.androidcapstone.symptommanagement.server.service.PatientServiceApi#addCheckin(PatientCheckin)
	 */
	@Override
	@RequestMapping(value=RestConstants.PATIENT_CHECKIN_REST_PATH, method=RequestMethod.POST)
	public PatientCheckin addCheckin(@RequestBody PatientCheckin checkin) {
		
		
		//insert checkin and checkin questions
		PatientCheckin newCheckin = patientCheckinRepository.save(checkin);
		
		//insert medication questions
		List<MedicationQuestion> questions = checkin.getMedicationQuestions();
		long checkinId = newCheckin.getCheckinId();
		for (MedicationQuestion medicationQuestion : questions) {
			medicationQuestion.setCheckinId(checkinId);
		}
		medicationQuestionRepository.save(questions);
		System.out.println("New Checkin: " + newCheckin);
		
		//Update patient alerts
		long patientId = checkin.getPatientId();
		List<PatientCheckin> checkins = patientCheckinRepository.findCheckinsByPatientId(patientId);
		DoctorAlarm alarm = alertManager.saveAlarmForLatestCheckin(checkins);
		System.out.println("Alarm saved (if not saved, object contains no data): " + alarm);
		
		return newCheckin;
	}
	
	
	@RequestMapping(value=RestConstants.PATIENT_FIND_MEDICATIONS_REST_PATH, method=RequestMethod.GET)
	public List<Medication> findMedications(@PathVariable(SymptomManagementPathVariable.Name.PATIENT_ID) long patientId) {
		
		return medicationRepository.findMedicationsByPatient(patientId);
	}
}
