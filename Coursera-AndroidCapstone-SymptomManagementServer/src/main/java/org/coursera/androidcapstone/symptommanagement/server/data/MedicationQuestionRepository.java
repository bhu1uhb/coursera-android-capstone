package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestionPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationQuestionRepository extends CrudRepository<MedicationQuestion, MedicationQuestionPK> { 

	@Query(value="select CHECKIN_ID, MEDICATION_ID, HAS_TAKEN_MEDICATION, DATE_TIME_TAKEN from app.CHECKIN_MEDICATION_QUESTION where CHECKIN_ID=?1 and MEDICATION_ID=?2", nativeQuery=true)
	public MedicationQuestion findMedicationQuestionByCheckinIdAndMedicationId(Long checkinId, Long medicationId);

	@Query(value="select CHECKIN_ID, MEDICATION_ID, HAS_TAKEN_MEDICATION, DATE_TIME_TAKEN from app.CHECKIN_MEDICATION_QUESTION where CHECKIN_ID=?1", nativeQuery=true)
	public List<MedicationQuestion> findMedicationQuestionsByCheckinId(Long checkinId);
}
