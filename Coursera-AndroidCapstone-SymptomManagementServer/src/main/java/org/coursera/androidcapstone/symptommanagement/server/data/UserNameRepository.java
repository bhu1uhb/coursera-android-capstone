package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.coursera.androidcapstone.symptommanagement.common.domain.UserName;


@Repository
public interface UserNameRepository extends CrudRepository<UserName, Long>{

	public List<UserName> findUserByFirstNameAndLastName(String firstName, String lastName);
	
}
