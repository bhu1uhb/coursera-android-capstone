-- Server-side database for Symptom Management app
--
-- Created using a networked server Derby ver 10.11.1.1
-- Commands to work with the Derby server: 
-- Start network server: startnetworkserver.bat
-- Stop network server: stopnetworkserver.bat
-- Connect with ij: connect 'jdbc:derby:symptommgmnt;create=true'
-- Credentials: username=symptommgmnt; pwd==password1

SET SCHEMA APP;
DROP TABLE checkin_medication_question;
DROP TABLE patient_medication;
DROP TABLE medication;
DROP TABLE checkin_questions;
DROP TABLE doctor_patient_relation;
DROP TABLE patient_alert;
DROP TABLE checkin;
DROP TABLE user_details;
DROP TABLE user_credentials;

-- Login credentials
CREATE TABLE user_credentials (
	user_id INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	login_name VARCHAR(15) NOT NULL, --user login 
	login_pwd VARCHAR(15) NOT NULL, -- user password
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
);

-- Holds details for users of Symptom Management app
-- Holds both doctor and patient users.
CREATE TABLE user_details (
	user_id INTEGER NOT NULL CONSTRAINT user_details_fk REFERENCES user_credentials(user_id), -- user_id of patient
	user_lastname VARCHAR(50) NOT NULL,
	user_firstname VARCHAR(30) NOT NULL,
	user_phone VARCHAR(12),
	user_email VARCHAR(40),
	user_birthdate DATE, --required only for patient 
	user_type CHAR(2) NOT NULL CONSTRAINT USER_TYPE_CK CHECK (USER_TYPE IN('PA','DR' )), --patient or doctor
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
) ;

-- Associate doctors with patients and vice versa.
-- Each user_type can have relations with another user_type,
-- but not the same user_type.
CREATE TABLE doctor_patient_relation (
	relation_id INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	patient_id INTEGER NOT NULL CONSTRAINT relation_patient_fk REFERENCES user_credentials(user_id), -- user_id of patient
	doctor_id INTEGER NOT NULL CONSTRAINT relation_doctor_fk REFERENCES user_credentials(user_id), -- user_id of doctor
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
);

-- Records a checkin triggered by a client notification.
-- Previous checkins will be compared to see if the patient
-- has reached a 'doctor alert' threashold using the checkin_datetime
-- timestamp.
CREATE TABLE checkin (
	checkin_id INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), 
	patient_id INTEGER NOT NULL CONSTRAINT checkin_patient_fk REFERENCES user_credentials(user_id), -- user_id of patient
	checkin_datetime TIMESTAMP NOT NULL,
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
);

-- Checkin questions not related to medications.
-- The mod_date between checkins will be used to determine whether
-- a patient's doctor needs to be notified.
CREATE TABLE checkin_questions (
	checkin_id INTEGER CONSTRAINT checkin_question_fk REFERENCES checkin, 
	-- Pain values: WLC=Well-Controlled; SEV=Severe; MOD=Moderate
	mouth_throat_pain CHAR(3) NOT NULL CONSTRAINT throat_pain_ck CHECK (mouth_throat_pain in ('WLC', 'SEV', 'MOD')) DEFAULT 'WLC',
	does_stop_eat_drink CHAR(1) NOT NULL CONSTRAINT stop_eat_drink_ck CHECK (does_stop_eat_drink in ('N', 'S', 'C')) DEFAULT 'N',
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
) ;

-- Holds medication data.
-- ???Doctor would add new medication
CREATE TABLE medication (
	medication_id INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),  
	medication_name VARCHAR(200) NOT NULL,
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
) ;


-- Patient medication list maintained by his/her doctor.
-- A patient could have more than one active medication.
CREATE TABLE patient_medication (
	patient_id INTEGER NOT NULL CONSTRAINT patient_med_fk REFERENCES user_credentials(user_id), -- user_id of patient
	medication_id INTEGER NOT NULL CONSTRAINT patient_medication_fk REFERENCES medication,
	--if a doctor removes a medication from the patient's list, the is_active flag will be set to 'N' and the
	--data for this record will only be used in historical views shown to the doctor 
	is_active CHAR(1) NOT NULL CONSTRAINT is_active_ck CHECK (is_active in ('Y', 'N')) DEFAULT 'Y',
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
);

-- Checkin questions for each pain medication.
-- A checkin will contain one record per medication in this table.
CREATE TABLE checkin_medication_question (
	checkin_id INTEGER CONSTRAINT checkin_med_question_fk REFERENCES checkin, 
	medication_id INTEGER NOT NULL CONSTRAINT medication_fk REFERENCES medication,
	has_taken_medication CHAR(1) NOT NULL CONSTRAINT taken_med_ck CHECK (has_taken_medication in ('Y', 'N')) DEFAULT 'N',
	date_time_taken TIMESTAMP, -- if has_taken_medication == 'Y (null otherwise)
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
) ;

-- Holds patient alerts calculated
-- on the serverside when checkin data is synched.
-- Alert criteria is if patient:
-- 		12 hours of severe pain (code=SEV)
--		16 hours of moderate to severe pain (code=MOD)
--		12 hours of 'I can�t eat' (code=CNT)
-- CHECKIN.CHECKIN_DATETIME column data will be used
-- to calculate time intervals for alerts.
-- When doctor is alerted through a notification, the DR_NOTIFIED flag will
-- be set to 'Y'.
-- Each record records one alert.
create table patient_alert (
	alert_id INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	checkin_id INTEGER NOT NULL CONSTRAINT patient_alert_checkin_fk REFERENCES checkin(checkin_id),
	patient_id INTEGER NOT NULL CONSTRAINT patient_alert_patient_fk REFERENCES user_credentials(user_id), -- user_id of patient,
	alert_code CHAR(3) NOT NULL CONSTRAINT alert_id_ck CHECK (alert_code in ('SEV', 'MOD', 'CNT')) DEFAULT 'N',
	dr_notified CHAR(1) NOT NULL CONSTRAINT dr_notified_ck CHECK (dr_notified in ('Y', 'N')) DEFAULT 'N',
	mod_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- datetime when record was inserted or last updated
);
