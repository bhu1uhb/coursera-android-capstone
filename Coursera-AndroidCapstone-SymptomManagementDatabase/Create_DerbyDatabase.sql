DROP TABLE patient_symptom_result;
DROP TABLE medication;
DROP TABLE patient_medication;
DROP TABLE patient_symptom;
DROP TABLE checkin;
DROP TABLE smgmnt_user;

CREATE TABLE smgmnt_user (
	user_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	user_name VARCHAR(50) NOT NULL,
	user_phone VARCHAR(12),
	user_email VARCHAR(40),
	user_type CHAR(2) NOT NULL CONSTRAINT USER_TYPE_CK CHECK (USER_TYPE IN('PA','DR' )) --PA or DR
	CONSTRAINT smgmnt_user_pk PRIMARY KEY (user_id)
) ;

CREATE TABLE checkin (
	checkin_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), 
	pat_user_id INTEGER NOT NULL,
	dr_user_id INTEGER NOT NULL,
	sympton_id INTEGER NOT NULL,
	symptom_result_id INTEGER NOT NULL 
	-- TOD: checkin data columns
	CONSTRAINT checkin_pk PRIMARY KEY (checkin_id)
) ;

CREATE TABLE patient_symptom (
	symptom_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	medication_name VARCHAR(200) NOT NULL
	CONSTRAINT symptom_pk PRIMARY KEY (symptom_id)
) ;

CREATE TABLE patient_symptom_result (
	symptom_result_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	result_name VARCHAR(200) NOT NULL
	CONSTRAINT symptom_result_pk PRIMARY KEY (symptomresult_id)
) ;

CREATE TABLE patient_medication (
	pat_meds_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),  
	pat_user_id INTEGER NOT NULL,
	dr_user_id INTEGER NOT NULL,
	medication_id INTEGER NOT NULL  		
	CONSTRAINT pat_medication_pk PRIMARY KEY (pat_meds_id)
) ;

CREATE TABLE medication (
	medication_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),  
	medication_name VARCHAR(200) NOT NULL
	CONSTRAINT medication_pk PRIMARY KEY (medication_id)
) ;

